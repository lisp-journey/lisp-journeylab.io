---
title: "Pretty GUIs now: nodgui comes with a pre-installed nice looking theme"
date: 2023-06-01T19:03:35+02:00
draft: false
tags: ["gui",]
---

Being able to load a custom theme is great, but it would be even
better if we didn't have to manually install one.

Well, recent changes in [nodgui](https://notabug.org/cage/nodgui) from
yesterday and today just dramatically improved the GUI situation for
Common Lisp[0].

## nodgui now ships the yaru theme

@cage commited the [Yaru theme from ttkthemes](https://ttkthemes.readthedocs.io/en/latest/themes.html#yaru) in nodgui's repository, and we added QoL improvements. To use it, now you can simply do:

~~~lisp
(with-nodgui ()
  (use-theme "yaru")
  …)
~~~

or

~~~lisp
(with-nodgui (:theme "yaru")
  …)
~~~

or

~~~lisp
(setf nodgui:*default-theme* "yaru")
(with-nodgui ()
  …)
~~~

Yaru looks like this:

!["yaru theme list box and buttons"](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-yaru.png)

!["yaru theme treeview"](https://raw.githubusercontent.com/vindarel/ltk-tests/master/treeview-yaru.png)

!["yaru theme"](https://imgur.com/I4EYDSA.png)

No, it isn't native, but it doesn't look like the 50s either.

See my [previous post](https://lisp-journey.gitlab.io/blog/pretty-gui-in-common-lisp-with-nodgui-tk-themes/) for more themes, screenshots and instructions to load a third-party theme. Forest Light is nice too!


## Try the demos

Try the demos with this theme:

~~~lisp
(setf nodgui:*default-theme* "yaru")
(nodgui.demo:demo)
;; or
(nodgui.demo:demo :theme "yaru")
;; a precise demo
(nodgui.demo::demo-widget :theme "yaru")
~~~

## Themes directory

@cage also made it easier to load a theme.

> I have added the special variable `*themes-directory*` (default is the directory themes under the directory where the asdf system is) where the library looks for themes.

> Each theme must be placed in their own directory as a subdirectory of the aforementioned variable, the name of the directory must be the name of the theme; moreover the name of the TCL file that specify the file must be named as the same of the theme with the extension "tcl" appended

> For example, the theme "foo" has to be: "foo/foo.tcl"

> Provided these conditions are met using a new theme should be as simple as type `(nodgui:use-theme "foo")`, without `(nodgui: eval-tcl-file)`.

Otherwise, just clone a theme repository somewhere, and call

    (eval-tcl-file "path/to/the/theme.tcl")
    (use-theme "theme")

I can very well imagine using small GUI tools built in Tk and this theme. I'll have to try nogui's auto-complete widget too. If you do build a little something, please share, it will help and inspire me and the ones after you.


- https://peterlane.netlify.app/ltk-examples/
- @cage announces new releases on Mastodon. `@cage@stereophonic.space`.

---

[0]: be more grandiose if you can.
