---
title: "Pretty GUI in Common Lisp with nodgui's Tk themes"
date: 2023-05-27T00:22:04+02:00
draft: false
tags: ["gui",]
---

Do you think Tcl/Tk GUIs are doomed to look outdated?

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/mediaplayer.png)

Fear not!

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-adapta.png)

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-yaru.png)

![Forest light theme](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-forest-light.png)

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-aquativo.png)

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-breeze.png)

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-clearlooks.png)

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-radiance.png)

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/media-lightbrown.png)

A treeview widget:

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/treeview-forest.png)

![](https://raw.githubusercontent.com/vindarel/ltk-tests/master/treeview-yaru.png)


The official example of [Forest Light](https://github.com/rdbende/Forest-ttk-theme):

![](https://raw.githubusercontent.com/rdbende/Forest-ttk-theme/master/Forest-light%20screenshot.png)

The [ttkthemes gallery](https://ttkthemes.readthedocs.io/en/latest/themes.html)

Plus, Tk itself has a little choice of built-in themes:

![](https://lispcookbook.github.io/cl-cookbook/assets/gui/ltk-on-macos.png)


---

We can use these themes with [nodgui](https://notabug.org/cage/nodgui), the Ltk fork.

In June of 2020, @cage added a little function to load a .tcl file:

```lisp
(defun eval-tcl-file (file-path)
  "This function will feed the TCL interpreter with the contents
   of the file `path'.
   Please, as this function will load  and execute a script, ensure to
   load files only from trusted sources otherwise severe security problem
   may arise."
  (assert (stringp file-path))
  (format-wish "source {~a}" file-path))
```

As a consequence, we can load a .tcl script that defines a theme, and use it. Themes generally consist of a .tcl script and a directory of png or gif images (when images are not defined in-line).

Considering we cloned the ttkthemes repo locally:

~~~lisp
  (with-nodgui ()
    (eval-tcl-file "ttkthemes/ttkthemes/png/yaru/yaru.tcl")
    (use-theme "yaru")
~~~

and that's all there is to it.

---

For now, some themes are not supported. Scalable themes are not
supported, the .gif based themes of ttkthemes won't load (the "scid"
and "smog" themes in ttkthemes, the [Sun Valley
theme](https://github.com/rdbende/Sun-Valley-ttk-theme) didn't
work). This could change when `tksvg` lands in Debian (or maybe, if
you install it yourself? I didn't try), or with the next release of
Tcl/Tk that will include SVG support (read
[#13](https://notabug.org/cage/nodgui/issues/13)).


Frankly, that was a great news of the day. Yes, I think some themes
are pleasant to the eyes! This makes me want to use little Tk UIs here
and there.

[Here's the code](https://github.com/vindarel/ltk-tests/blob/master/musicplayer-nodgui.lisp) for the little media player of the screenshots. It is based on [Peter Lane's extensive examples](https://peterlane.netlify.app/ltk-examples/).

Kuddos to @cage o/
