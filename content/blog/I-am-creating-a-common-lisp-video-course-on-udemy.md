---
title: "I Am Creating a Common Lisp Video Course on Udemy (free video previews) 🎥"
date: 2022-04-15T15:23:14+02:00
tags: ["tutorial", "product"]
draft: false
---

Everyone, let me celebrate a little bit: I am creating a [Common Lisp
video course on the Udemy
platform](https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358). I'm
several dozen hours in already and it's taking a good shape! It is so
much more time consuming to create videos than to write a tutorial O_o
But I like what's in there already, although there isn't everything I
want to teach, of course. I'm working on more content. Everything will
come in time, and meanwhile you can buy the course: you'll get future
content for "free" ;) Yes the course is to sell, hopefully it will
help me concentrate more on my CL activities (BTW, dear reader, here's
[a 50% off
coupon](https://www.udemy.com/course/common-lisp-programming/?couponCode=2204-LISP4ALL)
for April, 2022, and if you are a student drop me a line). Currently 4
videos are freely viewable, and I'm also posting new videos on
Youtube (that one is on [how to create a new Common Lisp project](https://www.youtube.com/watch?v=XFc513MJjos) with my project generator) but more on that
later. You currently have more than 3 hours of learning material.

You probably know me from my blog and my Lisp activities. I am
[vindarel](https://github.com/vindarel/), I contribute to
community-based learning resources such as the
[Cookbook](https://lispcookbook.github.io/cl-cookbook/) and I use CL
in production©. Honestly, I missed a Cookbook-like resource as a
beginner. I dumped there a lot of content and must-know tricks that I
either learned the hard way, either learned by chance. But **I still
want Common Lisp to be easier to learn** and, for that, there is the
video media. Consequently, **I truely think this course is today's
most efficient way to learn Common Lisp**.

So, what can you learn already in my course?

I want it to be practical: you will learn Lisp the language in order
to build real-world stuff.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->

- [Chapter 1 is how to get started](#chapter-1-is-how-to-get-started)
- [Chapter 2 is about Lisp basics](#chapter-2-is-about-lisp-basics)
- [Chapter 3 is about iteration. It is made of shorter videos that typically sum up in 5 minutes](#chapter-3-is-about-iteration-it-is-made-of-shorter-videos-that-typically-sum-up-in-5-minutes)
- [Chapter 4 teaches everything you need to know about functions](#chapter-4-teaches-everything-you-need-to-know-about-functions)
- [Chapter 5 shows how to work with projects](#chapter-5-shows-how-to-work-with-projects)
- [Who is this course for by the way?](#who-is-this-course-for-by-the-way)
- [Closing thoughts](#closing-thoughts)
- [ADDENDUM: the Lisp philosophy revealed](#addendum-the-lisp-philosophy-revealed)

<!-- markdown-toc end -->

<!-- ![](/images/udemy-photo-laptop--lambda.png) -->
<a href="https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358">
  <img src="/images/udemy-photo-laptop--lambda.png" alt="Udemy" width="750"/>
</a>


## Chapter 1 is how to get started

1.1. We start by **installing SBCL** on our machine (showed for Unix,
   links for Windows). This one is 15m long and is 🆓 free to
   watch. We see how to start our Lisp, how to write "hello world", we
   understand the output, we add readline support to the SBCL default
   REPL in the terminal, we disable the interactive debugger, and we
   have a few words on Lisp implementations and GNU CLISP in
   particular.

1.2. We see **how to run Lisp code**, the simplest way. We write a code snippet *with a simple
   text editor* and we run it with sbcl's `--script` and `--load` flags. We
   use the `LOAD` function.

1.3. We see **how to use Portacle** (the ready-to-use, multiplatform
image shipping Emacs, SBCL, Quicklisp, Git and a couple handy Emacs
packages).


## Chapter 2 is about Lisp basics

2.1. I heard Lisp beginners who needed **a recap on the Lisp syntax
and the evualation model**. This one is also 🆓 available to everyone. We
see: the prefix notation, that everything is an expression, the
evaluation model (and the exception of macros). Code is data is code… right?

2.2. How to define **variables**, at the toplevel or locally. How to
lexically re-bind dynamic variables, the gotcha, the alternative.

2.3. and **conditionals** (if, when, #+or…)

That's it for now for this chapter (yes, we'll see data structures, but for now I refer you to the Cookbook, on the page that I also authored. You are armed to read it.).

## Chapter 3 is about iteration. It is made of shorter videos that typically sum up in 5 minutes

…what took me a long time to learn or discover, a way longer time to admit.

3.1 **Iterating over lists and vectors** (with 🆓 free preview). `loop`, `dolist` and other libraries of the ecosystem.

3.2 then: **Iterating over a hash-table** keys and values. We see 5 different ways in 5 minutes.

3.3 **Iterating a fixed or infinite number of times**, and we take the
opportunity to build our first read-eval-print-loop.

3.4. Here, we take a **high level overview of `loop`** and we study some gotchas. We see a practical example from an answer to last year's Advent Of Code.


## Chapter 4 teaches everything you need to know about functions

(with a sneak peak into CLOS):

4.1. **How to create named functions**, how to handle all types of arguments (🆓 free preview). We see `defun`, returned values, required arguments, optional arguments, key arguments, how to set a default value, how to know if an argument was supplied, `&rest`, example of `apply`, feature flags…

4.2. Referencing functions, redefining functions locally, accessing documentation

4.3. **Multiple Return Values** (they are NOT like returning a list or a tuple!!!)

4.4. **Higher Order Functions**: how to give functions as arguments, `member`, the `:test` keyword, `map` and `mapcar`, `lambda`, how to generate functions, what are symbols, setf symbol-function. A word on currying and being a Lisp-2.

4.5. **Closures**

4.6. setf functions

4.7. **Generic Functions** (quick intro to CLOS): they allow to write functions that dynamically dispatch on the type of their arguments. What we see (quickly): `defmethod`, `defgeneric`.

and, lastly (for now):

## Chapter 5 shows how to work with projects

<!-- How to create a project from scratch or how to work with an existing project. -->

5.1 **How to work with an existing project**. ASDF, Quicklisp, SLIME shortcuts, some useful `asdf` functions…

5.2. **How to create a new project**. As a bonus, see again my new
video on Youtube: [Common Lisp: how to create a new
project](https://www.youtube.com/watch?v=XFc513MJjos) (demo of my
project generator).

5.3. What are **systems and packages** anyways? Demo. Gotcha when creating a package. Finding all external symbols of a package.

---

Soooo

## Who is this course for by the way?

I must warn that this course is *not* for total newcomers in programming. You should know what variables and functions are. You should knew that Common Lisp is a language of the Lisp family! (I tell more in the presentation video, but still).

Lisp newbies are welcome because I introduce Lisp basics (syntax,
evaluation model) and I show how to install everything. It would
help if you know what is a language of the Lisp family.

This course is mainly targetting young(ish) profesional developers
like me, who feel they deserve a more fun, comfy, compiled and fast
programming language.

It is for Python or JavaScript programmers frustrated by the unstability of their ecosystem,

for students of computer science who want to discover why Lisp still has un-matched alien technology inside (and maybe for \*your\* students?),

for Clojurists who want to transition quickly to a bare-metal Lisp,

or simply for your friend or colleague to whom you are trying to sell the power of Lisp ;)

<!-- If you want Common Lisp to continue to strive, help the guys who help doing it :D -->

## Closing thoughts

If you are already a programmer: you can watch the videos at speed 1.25 or higher, but try to not skip content. You can start by the chapter of your choice. Use the captions, they are manually edited.

Thanks kindly for your support in that new journey of mine! Hope you'll enjoy the content (I know you'll learn a few tricks).

<p >
 <strong style="box-shadow: none"> Learn Common Lisp now! It's a tool for a lifetime.
 </strong>
 </p>

<p >
 <strong style="box-shadow: none"> And have fun!
 </strong>
 </p>

<!-- => [https://www.udemy.com/course/common-lisp-programming/](https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358) -->

<p class="announce"
         style="background-color: #f48224;
    color: white;
    padding: 1em;
    margin: 1em 0 1em 0;
    text-align: center;
    opacity: 0.9;
">
           🎥  

  <strong style="box-shadow: none">
        <a href="https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358" style="color: white">
        Common Lisp: from novice to effective developer
        </a>
  </strong>
           🎥
</p>


---


- [project's GitHub](https://github.com/vindarel/common-lisp-course-in-videos/)
  - request: [debugging chapter](https://github.com/vindarel/common-lisp-course-in-videos/issues/3)
- a known issue: I started with a meh microphone. I bought a new one, sound is now good, but older videos were not re-captured yet.


## ADDENDUM: the Lisp philosophy revealed

Udemy auto-generates captions for your videos. You can manually edit
them… but sometimes I was tempted not to, as they can reveal some hidden
truth^^


> I can see my new prince here

reveals my respect for "print".

> Survivor needs votes.

I never thought about that O_o (that's for "the variable name is unbound")

CL might be more active than you think:

> Google and China's also actively contributes, as we said.

for "Google engineers also actively contribute to the SBCL implementation". My cute accent is well interpreted beyond hopes (and it is not wrong, hello fellow chinese lispers o/ ).

> and yes, I use communism prediction.

"I use CL in production"…

Lisp is used in more places than you think:

> I want to give you a few tips for when you are dealing with cruise people

aka "for when you are dealing with macros". And yes:

> my crew is special

indeed :p "the loop macro is special".

We knew a so called Lisp curse… there's a conspiracy too!

> the conspiracy control key

is "C-c C-k".

> and we can create with controversy

instead of "and we can quit with C-c C-c".

> you have to respect the water

you have to respect the order.

> discipline is nested in your code.

for "deeply nested in your code"

> I'll show you from Oslo

= "I'll show you from Emacs and Slime", that's quite an interpretation but Emacs is a great place to live in too.

And I also like that one:

> I'm actually a terrific

that is the translation of the name "Eitaro Fukamachi". Add "coder" and that matches :)
