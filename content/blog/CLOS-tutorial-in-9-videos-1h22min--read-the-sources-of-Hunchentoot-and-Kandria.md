---
title: "CLOS tutorial: I published 9 videos (1h 22min) on my course. You'll know enough to read the sources of Hunchentoot or the Kandria game 🎥 ⭐"
date: 2024-12-20T18:01:14+01:00
tags: ["tutorial",]
draft: false
---

This is a follow-up from [yesterday's post on reddit](https://www.reddit.com/r/lisp/comments/1hh7bgm/clos_intro/) and an announce I wanted to make since this summer: **I created 9 videos on CLOS, for a total of 1 hour and 22 minutes**, in which you learn what I detail below. You can [watch the course and subscribe here (Christmas coupon)](https://www.udemy.com/course/common-lisp-programming/?couponCode=LISP-EVERYWHERE-2025) and [learn more on GitHub](https://github.com/vindarel/common-lisp-course-in-videos/). The whole course is made of 51 videos divided in 9 chapters, for a total of 7 hours and 12 minutes. It is rated 4.71 / 5 as of date (thank you!!).

![](https://github.com/vindarel/common-lisp-course-in-videos/raw/master/announce.svg)


Yesterday was a great day because I received nice feedback:

> It is an amazing tutorial. What is really strange is I thought CLOS was complicated. I guess it can be but [Vincent] is amazing at explaining everything and demystifying it.
>
>    /u/intergalactic_llama

🔥 I appreciate any (constructive ;) ) feedback and positive ones a lot.

> Oh hey you made that tutorial. I started it but then got distracted by other stuff, been meaning to restart it and make my way through the whole thing. Really liked what I went through (I was on video 12 about redefining functions locally etc).
>
>    /u/runevault

Look, other recent feedback on my course:


> I have done some preliminary Common Lisp exploration prior to this course but had a lot of questions regarding practical use and development workflows. This course was amazing for this! I learned a lot of useful techniques for actually writing the code in Emacs, as well as conversational explanations of concepts that had previously confused me in text-heavy resources. Please keep up the good work and continue with this line of topics, it is well worth the price!
>
>    Preston, October 2024


<div class=""> </div>

> The instructor shows lots of tricks.
>
>    Tom, November 2024

<div class=""> </div>

> Excellent selection of content. The delivery is not always obvious just for watching, but when I do the examples, it’s absolutely clear that what I need to be learning has been presented.
>
>    Steven, November 2024 <3


<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Chapter content](#chapter-content)
    - [1. `defclass`, `make-instance`, `slots`… aka CLOS crash course, part 1. This one is free to watch 🆓](#1-defclass-make-instance-slots-aka-clos-crash-course-part-1-this-one-is-free-to-watch-🆓)
    - [1b. Quizz: CLOS crash test](#1b-quizz-clos-crash-test)
    - [2. Inheritance, multimethods, around, before and after methods… aka CLOS crash course, part 2](#2-inheritance-multimethods-around-before-and-after-methods-aka-clos-crash-course-part-2)
    - [3. Pretty printing](#3-pretty-printing)
    - [4. `defclass` review](#4-defclass-review)
    - [5. Custom constructors and custom logic.](#5-custom-constructors-and-custom-logic)
    - [6. `initialize-instance`: control if and how any objects are created](#6-initialize-instance-control-if-and-how-any-objects-are-created)
    - [7. Multiple inheritance](#7-multiple-inheritance)
    - [8. `defgeneric` vs `defmethod`: when to use which, which is better?](#8-defgeneric-vs-defmethod-when-to-use-which-which-is-better)
    - [8b. Quizz: reading code from real-world projects.](#8b-quizz-reading-code-from-real-world-projects)
    - [9. Class allocation](#9-class-allocation)
- [Outcome](#outcome)
- [Closing words](#closing-words)

<!-- markdown-toc end -->


# Chapter content

## 1. `defclass`, `make-instance`, `slots`… aka CLOS crash course, part 1. This one is free to watch 🆓

We see in more details: `defclass`, `make-instance`, attributes (aka slots), slot options (initarg, initform, reader, writer, accessor, documentation), slot-value, generic functions, `defmethod`, **dispatching** on built-in types, how objects are lazily updated, **Slime inspector** actions, manipulating Slime **presentations**, unbound slots and `slot-boundp`, Slime shortcuts to create objects…

We see a LOT already in this video, in an efficient way (way more
efficient than when I learned anyways), so if you're on a budget you
can start with it (it's free to watch) and complement with the
Cookbook, and the other free books. Also if you are a student shoot me
an email (and avoid the reddit chat, I don't see the notifications, sorry
about that).


## 1b. Quizz: CLOS crash test

There is a small quizz. Keep in mind that the Udemy plateform doesn't support any Lisp language so I can't put any live coding exercises, but we can read code.

## 2. Inheritance, multimethods, around, before and after methods… aka CLOS crash course, part 2

what we see more precisely: inheritance, multimethods, :around, :before and :after methods (think signals and overwriting default methods in other languages, that allow to control what happens when a method is called, if it is called at all), their order of execution, a Slime shortcut to export all symbols of a class at once…

## 3. Pretty printing

We see how to change the default printed representation of objects.

What we see: `print-object`, with print-unreadable-object, the object type, the object identity, classic gotchas.

You know, normally an object is printed un-readable as

    #<ROBOT {1005CEBD03}>

(guess what AOC day I am at)

and we can use the `print-object` *method* to print it however we like, such as

    #<ROBOT x: 47 y: 14 {1005CEBD03}>


## 4. `defclass` review

We give another pass, slower, to `defclass`, slot options, `make-instance`, and to the fact that **accessors are generic functions**.

You can skip this one if the crash course was crystal clear.

## 5. Custom constructors and custom logic.

What we see: writing our own "make-person" terse constructor. Adding some logic before the object creation, doing side-effects after the object creation: towards `initialize-instance`.

## 6. `initialize-instance`: control if and how any objects are created

What we see: defining a `:before` and an `:after` method of `initialize-instance` for our person class, in order to do the same logic than with our custom constructor, but with a built-in CL Object System mechanism. Note that using INITIALIZE-INSTANCE isn't a must, only a "can", that you can use for your own classes, or to control the creation of objects from other systems.


## 7. Multiple inheritance

What we see: how to **inherit** from multiple parent classes and who takes precedence, when the parents define the same slot with each a default value. Quick illustration. We use what is known as a **mixin class** to add functionality to our class.

## 8. `defgeneric` vs `defmethod`: when to use which, which is better?

What we see: the use of `defgeneric` and `defmethod`, either separately, either together.
`defgeneric` has a couple advantages in regards to documentation and keeping your code in sync with your image.

## 9. Class allocation

What we see: the default `:allocation :instance` VS `:allocation :class`. How to automatically count how many objects of a class are created.

## 8b. Quizz: reading code from real-world projects.


# Outcome of the chapter

There was a lot of choices to make and advanced topics to ignore for
this first chapter on CLOS. What drove my choices was looking at
real-world code out there. As a result, by the end of this chapter,
you will **know enough to read real-world Common Lisp projects** such
as the Hunchentoot **web server** or [the Kandria **game**](https://github.com/Shirakumo/kandria/). Bravo!


# Closing words

First of all, thank you for your encouragements, and to everyone who
took the course or who shared it!

Today I'd like to answer to my past me, a newcomer to Lisp on a
budget: why create a paying course? First of all, I still contribute
to the Cookbook, a collaborative resource. It's not "free or paid"
resources, it's both. Then, preparing and recording structured videos
takes so much time that I wouldn't do this continuous effort if I hadn't the
ambition to make a non-ridiculous hourly rate on them one
day. Disclaimer: it isn't the case yet. Maybe next year, depending on
how many videos I release ;) I can pay my rent with them once every
few months though, that's cool. Rest assured I'm not a
millionaire. I'm on my own projects and I don't have a fixed (nor big)
income. So your contribution [or sponsorship](https://github.com/sponsors/vindarel/) counts,
if only for the good vibes that push me to spend more and more time on my
growing list of projects.

 <!-- So, by the way, the CLOS chapter on the Cookbook is pretty complete IMO, it's only lacking advanced MOP. -->

You can sponsor [other lispers](https://github.com/lisp-maintainers/lisp-maintainers) too.

Thank you and happy lisping.
