---
title: "New resource specialized on web development in Common Lisp"
date: 2025-01-15T10:39:53+01:00
draft: false
tags: [web, tutorial]
---

I just released a new documentation website specialized on web development in Common Lisp:

- 🚀 [https://web-apps-in-lisp.github.io/](https://web-apps-in-lisp.github.io/)

I'd be embarrassed to tell how long it took me to grasp all the
building blocks and to assemble a resource that makes sense. I hope it
serves you well, now don't hesitate to share what you are building, it
creates emulation!


In the first tutorial we build a simple app that shows a web
form that searches and displays a list of products.

![](https://web-apps-in-lisp.github.io/tutorial/web-app.png)


We see many necessary building blocks to write web apps in Lisp:

- how to start a server
- how to create routes
- how to define and use path and URL parameters
- how to define HTML templates
- how to run and build the app, from our editor and from the terminal.

In doing so, we'll experience the interactive nature of Common Lisp.

In the user log-in section, we build a form that checks a user name and a password:

![](https://web-apps-in-lisp.github.io/building-blocks/login.png)

We also introduce databases, and more topics.

The sources are here: [https://github.com/web-apps-in-lisp/web-apps-in-lisp.github.io/](https://github.com/web-apps-in-lisp/web-apps-in-lisp.github.io/) and the GitHub Discussions are open.
