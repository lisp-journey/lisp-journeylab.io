---
title: "Celebrating 1001 learners on my Common Lisp course 🥳"
date: 2024-02-14T17:05:44+01:00
draft: false
---

I just got [1001 learners on my Common Lisp
course](https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358)
on Udemy. Thanks everybody for your support, here or elsewhere!

Starting with CL was honestly not easy. The first thing I did was
writing the "data structures" page on the Cookbook, bewildered that it
didn't exist yet. A few years and a few projects later, this course
allows me to share more, learn more, have fun, and have some rewards
to keep the motivation up.

<img src="/images/udemy-course.png" style="max-width: 1000px" href="https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358" title="Learning Common Lisp in videos on my Udemy course."/>

I know the course isn't complete by any means, I want to add many
chapters, both advanced topics but easier material for newcomers as
well (beware, my course isn't for total beginners in a Lisp
language). The next one, and soon©, will be all about CLOS. In the
meantime, I don't abandon you, I enhance the Cookbook, I publish some
videos on Youtube ([last one: web development in Common Lisp, part 1
and part 2](https://www.youtube.com/watch?v=h_noB1sI_e8)), I work on
starter kits or on newcomer-friendly libraries ;)

Thanks again,

don't hesitate to share the link with a friend or a colleague ;)

Here's a [link with a coupon until March, 13th 2024](https://www.udemy.com/course/common-lisp-programming/?couponCode=CELEBRATE1001). (student? Get in touch for a free link)

- [more information on Github](https://github.com/vindarel/common-lisp-course-in-videos/)


<iframe src="https://emacs.ch/@louis/111937239776148099/embed" width="400" allowfullscreen="allowfullscreen" sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox allow-forms"></iframe>