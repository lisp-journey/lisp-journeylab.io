---
title: "Lisp Interview Quantum Computing"
date: 2024-08-12T19:06:46+02:00
<!-- draft: false -->
draft: true
---

Dear lisper hello,


How are you doing?
Do you still want to talk about using CL in a team? It's about time we kill some FUD!

I will publish the interview on my lisp-journey blog
https://lisp-journey.gitlab.io/tags/companies/
where we see I had a good start but it's been a long time I didn't interview a company (well, some don't answer).

We can adopt a very liberate approach: add questions, ignore others, refer me to wall of text you wrote somewhere and I'll add them if you want to save time, etc.

It's also important the page looks attracting and has more than a wall
of text.  Do you think you would find cool images related to your
work, to CL in prod? It can simply be a screen capture of something
you are working on, of your CI pipeline, a photo of something around
your workplace…

Otherwise, I can add random images about quantum computing and your
company (HRL labs).

In advance, thanks!

--- [start of the interview] ----

# Lisp Interview: Robert Smith, managing a team of Common Lisp developers for quantum computing

<!-- or similar title -->

What's the experience of running an industrial-grade Common Lisp
project in production? How can one hire or train Lisp developers internally?

Robert Smith, aka `stylewarning`, has contributed many open-source
Common Lisp libraries and is active on social media (reddit, Twitter,
Discord etc), so you are probably familiar with some of his work or
have read some of his generous and expert answers.

For example, he is the co-creator [correct?] of
[Coalton](https://github.com/coalton-lang/coalton/), an important
library of the ecosystem which brings Haskell-like type checking on
top of Common Lisp, as a normal library (`quickload` it and you're
done), which is quite a achievement if you ask me. He tinkers with
bits and qubits during the day as he's working at [HRL
Laboratories](https://www.hrl.com/), *"one of the world's premier
physical science and engineering research laboratories"*, in the
quantum computing field, where he leads a team of (Common) Lispers.

He kindly accepted to answer my questions.

## Works

Q: Hi Robert, and thanks for joining us. How are you and what are you lisping on right now?

Q: Since we mentioned Coalton, could you tell us a bit about how this
library came to be? Why did you start it, how much of an effort did it
take (several years of exploration if I'm right?), do you develop it
with colleagues?

*For newcomers: Coalton is used "for real"© in their quantum stack.*


Q: Can you tell us a bit more what you work on? We find several projects under [quil-lang](https://github.com/quil-lang/), like `quilc`, "the optimizing Quil compiler". What does your company sell?

Q: Do you work exclusively on the Lisp projects, or does "the real (non-lisp) world" catches some of your attention?

Q: What's the history of Common Lisp at HRL Labs? Is it or was it used in other teams (departements?) than yours?

Q: I can cite three companies doing quantum computing and using Common Lisp (D-Wave, Rigetti, RHL Laboratories). Is Common Lisp all the rage in this space? Does the language have inner properties that make it a great fit?

Q: How large is the largest CL product you are working on?



## Tech stack

Q: What is your technological stack, Lisp or non-Lisp? From prototyping (if any) to running things in production©, what's involved?

Q: and the Lisp one now. What implementation(s) do you use? What libraries?

Q: How do you handle deployment? BTW, do you run services or ship software? Do you have a "dumb" process following the industry practices (it's a well kept secret that we can do simple things in CL), or do you rely on some CL special features? I'm thinking about connecting to a remote image in production, using core images…

*For the newcomers: you can choose your weapons in CL. Either deploy
from sources, the sources we keep in files and manage with a VCS, CL
doesn't force to use "big ball of muds". But we can also [connect to a
remote program and update it while it's
running](https://stackoverflow.com/questions/78897116/how-to-deploy-a-web-application-so-that-i-can-modify-its-code-while-it-is-runni). And
you can do everything in-between, for instance only explore data at
the REPL but don't touch code.*

Q: Did you have funny surprises in production, were you ever blocked because you "couldn't find answers on Stack-Overflow"? (I'm trying to kill some FUD here)

Q: In your experience, does the praised stability of Common Lisp (the language, the implementations, the libraries) live up to its promise?


## Common Lisp in teams

### Hiring, training

[it's a lot of questions, write as you want, really ;) ]

Q: You hired people to work on your Common Lisp team. You posted job
offers. Did you also find candidates without a proper job announce? [I
mean via other networks]

[I'm trying to show that no job announce doesn't mean no job opportunity]

Q: So, who do you hire: expert Lisp developers, mathematicians or phycisists, software engineers?

Q: Did you easily hire?

Q: Do you internally train your new coworkers to Common Lisp and how has it been going?

Q: How long do they need to be profficient in your code-base?

I can cite you, on reddit:

> One of my colleagues said something that resonated with me. He had no Lisp experience but turned out to be a fantastic Lisp developer. After reviewing some of his first Lisp PRs, I asked, "how is it possible that you're so good at Lisp this quickly without having written it ever before?" His response was something like, "I just try to pretend it's Haskell and Rust, and somehow that works." It's true. Understand that Lisp absolutely and unapologetically is not Haskell or Rust, but the frame of mind of those languages works well enough to be productive in Lisp, and gives a runway to learn the more nuanced, Lisp-specific patterns of programming (e.g., metaprogramming).

Q: Anything more on the topic?

[optional of course]


### Editors

Q: We might not avoid this hot topic: I figured that you tested
alternatives to Emacs and Slime, not for yourself but for the
workplace. How much of a difficulty does Emacs / Slime represent at
work, for who?

Q: Are any of the Atom/Pulsar, VSCode, Intellij, Sublime plugins good enough for your requirements yet? And Lem?

## Lasting words

Q: anything more?

Announces of cool stuff coming soon? Coalton features, new libraries…

shall we speak more about large codebases?
