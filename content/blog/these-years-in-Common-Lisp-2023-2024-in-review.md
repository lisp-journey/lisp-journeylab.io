---
title: "These years in Common Lisp: 2023-2024 in review"
date: 2025-02-17T19:06:46+02:00
draft: false
---

<!-- libraries [Project Mage: A Structural UI platform built in Common Lisp](https://project-mage.org/Campaign) -->


This is a personal pick of the most interesting projects, tools,
libraries and articles that popped-up in Common Lisp land in the last
two years.

Newcomers might not realize how the Common Lisp ecosystem, though
stable in many ways, actually evolves, sharpens, tries new solutions,
proposes new tools, ships new libraries, revives projects. And
everyone might enjoy a refresher.


Here's my [previous overview for 2022](https://lisp-journey.gitlab.io/blog/these-years-in-common-lisp-2022-in-review/).

The same warnings hold: I picked the most important links, in my
view, but this list is by no means a compilation of *all* new CL
projects or articles published on the topic. Look for yourself on Reddit,
Quicklisp releases, GitHub, and use your favourite search engine.

There are too many great news and achievements to pick 3. I love
what's happening around **SBCL** (and ECL, and Clozure's revival), I
love everything that got included into **Lem** and the work on all
other editors, I love the **webviews** and I love the **scripting**
tools that are emerging. What are your top picks?

OK, there's a news I want to put at the forefront: **HackerNews now runs on top of SBCL** ;)

---

If you are discovering the ecosystem, my recommendaton is to not miss
these two resources:

- [Awesome-cl](https://github.com/CodyReichert/awesome-cl) - a curated list of libraries (there might be more than you think)
  - if you are looking for a list of recommended libraries on each topic, look here.
- [the CL Cookbook](https://lispcookbook.github.io/cl-cookbook/)

Now let's dive in and thanks to everyone involved.


{{< rawhtml >}}
<a href="https://opusmodus.com/">
<img src="https://i.redd.it/ffb8gu6zobta1.png" style="max-width: 100%;" alt="The OpusModus music composition software." title="The OpusModus music composition software version 3, with a Windows version. A LispWorks product, not open-source, with demo videos below."/>
</a>
{{< /rawhtml >}}

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Community](#community)
- [Documentation](#documentation)
- [Implementations](#implementations)
    - [SBCL](#sbcl)
    - [ABCL](#abcl)
    - [CCL](#ccl)
    - [Allegro](#allegro)
    - [LispWorks](#lispworks)
    - [ECL](#ecl)
    - [CLASP](#clasp)
    - [SICL - the new, portable and modular implementation](#sicl---the-new-portable-and-modular-implementation)
    - [New implementations](#new-implementations)
    - [Historical: Medley Interlisp](#historical-medley-interlisp)
- [Companies and jobs](#companies-and-jobs)
- [Projects](#projects)
    - [Editors](#editors)
        - [About Emacs](#about-emacs)
        - [About VSCode](#about-vscode)
        - [About Lem and Rooms pair programming environment](#about-lem-and-rooms-pair-programming-environment)
        - [About LispWorks](#about-lispworks)
        - [About the Jetbrains plugin](#about-the-jetbrains-plugin)
        - [About Jupyter](#about-jupyter)
        - [Other tools](#other-tools)
    - [Coalton](#coalton)
    - [Package managers](#package-managers)
    - [Gamedev](#gamedev)
    - [GUI](#gui)
    - [Web](#web)
    - [Nyxt 4.0 pre-realease - now on Electron](#nyxt-40-pre-realease---now-on-electron)
- [More libraries](#more-libraries)
    - [Scripting](#scripting)
    - [Software releases](#software-releases)
- [Other articles](#other-articles)
- [Videos](#videos)

<!-- markdown-toc end -->



# Community

We could start with some reddit stats: [2025 - a New Year for an old programming language!](https://www.reddit.com/r/Common_Lisp/comments/1hr2omd/2025_a_new_year_for_an_old_programming_language/) (numbers are up).

The ELS team kept organizing the conference. We have a date and place for 2025: [European Lisp Symposium 2025 in Zürich, May 19/20](https://www.european-lisp-symposium.org/2025/index.html)

We saw new and regular [Lisp Ireland](https://lisp.ie/) [meetups](https://stripe.events/lispirelandatstripe).

Here's one of their videos: [Lisp Ireland, February 2024 Meetup - Lisp & Hardware Verification with ACL2](https://www.youtube.com/watch?v=iFEb9p54x_Q)

@djha-skin ran a survey, which is not an established practice in the community, and analysed the results: [Common Lisp Community Survey 2024 Results ](https://blog.djhaskin.com/blog/common-lisp-community-survey-2024-results/).


@shinmera (Yukari), the author of many useful libraries and an active member of the ELS, and even the host of the next one, [opened a Patreon. "If you'd like to help me continue my full-time open source Lisp work, please consider supporting me."](https://www.patreon.com/Shinmera). Sponsoring Yukari is money well spent. She is [on GH sponsors](https://github.com/sponsors/Shinmera) and [ko-fi](https://ko-fi.com/shinmera) too.

The community is on reddit, [Discord](https://discord.gg/hhk46CE), Mastodon, LinkedIn… and also on [XMPP](https://www.reddit.com/r/lisp/comments/1iazlrr/lisp_xmpp_channel/).


# Documentation

The **CL Cookbook** is a collaborative resource with new contributors each year: [new Cookbook EPUB and PDF release: 2025-01](https://github.com/LispCookbook/cl-cookbook/releases/tag/2025-01-09).

We got a great contribution: [Cookbook: Building Dynamic Libraries with SBCL-Librarian · by em7](https://lispcookbook.github.io/cl-cookbook/dynamic-libraries.html)

**PAIP** is a classic, now available on the web: [Peter Norvig: Paradigms of Artificial Intelligence Programming, Case Studies in Common Lisp (web version)](https://norvig.github.io/paip-lisp/#/).

[New resource: **Web Apps in Lisp: Know-how**](https://web-apps-in-lisp.github.io/index.html): I wanted a resource specialized for web development in Common Lisp. I mean to continuously extend it from now on.

I'll include a couple general videos in this section. More videos and more documentation improvements are to be found in their respective sections.

**FreeCodeCamp** released an extensive Common Lisp course on Youtube: [Lisp Programming Language – Full Course for Beginners - freeCodeCamp.org - Youtube](https://www.reddit.com/r/Common_Lisp/comments/1i1e766/lisp_programming_language_full_course_for/).

David Botton of CLOG fame released more beginner material, among which [Common Lisp - The Tutorial - Fast, Fun and Practical (with CLOG)](https://www.reddit.com/r/lisp/comments/196lain/common_lisp_the_tutorial_fast_fun_and_practical/).

I carry on the work on my **Common Lisp course** in videos, on the Udemy platform. Lately, I worked on a [CLOS tutorial: I published 9 videos (1h 22min) on my course. You'll know enough to read the sources of Hunchentoot or the Kandria game 🎥](https://lisp-journey.gitlab.io/blog/clos-tutorial-in-9-videos-1h22min--read-the-sources-of-hunchentoot-and-kandria/) [comments](https://www.reddit.com/r/lisp/comments/1hiraze/clos_tutorial_i_published_9_videos_1h_22min_on_my/). The course is comprised of more than 7 hours of short videos, with a code first approach, divided in 9 chapters. We see some basics but we quickly dive into more advanced Common Lisp topics. You can learn more about it [here on GitHub](https://github.com/vindarel/common-lisp-course-in-videos/). Students can send me an email for a free link.

Here's the feedback of redditors:

> I can vouch for the Udemy course. From the very first lesson, just firing up the REPL and Emacs/SLIME I was taught something new. It's a great course.

fuzzmonkey35, January 2025 (reddit)

> It is an amazing tutorial. What is really strange is I thought CLOS was complicated. I guess it can be but Vincent is amazing at explaining everything and demystifying it.

intergallactic_llama, January 2025 (reddit)

;)


# Implementations

Great times for Common Lisp implementations.

## SBCL

SBCL [ships monthly releases](https://www.sbcl.org/news.html). You really should look at and appreciate all the activity and the continous improvements.

One noticeable addition: its **new garbage collector**. [SBCL: merge of the mark-region GC](https://www.reddit.com/r/Common_Lisp/comments/15nmv0h/sbcl_merge_of_markregion_gc/).

More improvements include:

- "the mark-region parallel garbage collector can be enabled on arm64. (Thanks to Hayley Patton)",
- new contrib module sb-perf, "a performance-analysing tool for Linux. (thanks to Luke Gorrie and Philipp Marek)"
- support for cross-compiling the system to Android has been added (thanks to Gleefre)
- "support for memory allocation arenas is now available on the arm64 platform."
- haiku support
 - [Porting Common Lisp to Haiku OS](https://discuss.haiku-os.org/t/make-ansi-common-lisp-available-on-haiku-again/15780)
- sb-simd improvements

More good stuff with SBCL:

- [Porting SBCL to the **Nintendo Switch**](https://reader.tymoon.eu/article/437)
- [SBCL as part of an **Android** application!](https://www.reddit.com/r/Common_Lisp/comments/12pxuja/sbcl_as_part_of_an_android_application/)
- [Simple REPL App. (SBCL, Android - WIP)](https://www.reddit.com/r/Common_Lisp/comments/13ajpp0/simple_repl_app_sbcl_android_wip/)
- [40ants/tree-shaker: experimental tree shaker for SBCL](https://github.com/40ants/tree-shaker)
- [SBCL can now be installed on Windows via **Chocolatey** (unofficial)](https://community.chocolatey.org/packages/sbcl)
- [sbcl-builds: **Nightly builds of SBCL** for Windows using MSYS2 UCRT64.](https://github.com/olnw/sbcl-builds)
- [sbcl-goodies: distributing binaries with Common Lisp and foreign libraries. libssl, libcrypto and libfixposix are statically baked in.](https://blog.cddr.org/posts/2023-02-20-distributing-binaries-cl-ffi/)


{{< rawhtml >}}
<!-- <figure> -->
  <a href="https://reader.tymoon.eu/article/437">
    <img src="https://filebox.tymoon.eu//file/TWpjNU5nPT0=" style="max-width: 50%; margin: 2em;" alt="SBCL on the Nintendo Switch" title="SBCL with the Trial game engine on the Nintendo Switch."/>
 </a>
    <!-- <figcaption>Porting SBCL to the Nintendo Switch</figcaption> -->
<!-- <figure> -->
{{< /rawhtml >}}

There are open bounties to improve SBCL:

* [**$2000 USD bounty** to see by-value struct passing implemented in SBCL's native FFI.](https://www.reddit.com/r/Common_Lisp/comments/1hncabr/2000_usd_bounty_to_see_byvalue_struct_passing/)
  * it may be more than $2000 USD now.
  * You wouldn't start from zero, there is existing work. See the thread.
* another $700+ bounty to add **coroutines** in SBCL
  * same link. No official bounty page yet, I may work on it.

## ABCL

New release: [ABCL 1.9.1 "never use a dot oh": CFFI compatibilities, Java virtual threads, ASDF 3.3.6, fixed loading of Fricas0 and Maxima…](https://abcl-dev.blogspot.com/2023/02/abcl-191-never-use-dot-oh.html)

New release [ABCL 1.9.2](https://abcl.org/release-notes-1.9.2.shtml).

New tool: [Announcing the First Release of abcl-memory-compiler - Now Available!](https://www.reddit.com/r/lisp/comments/1eqvwnb/announcing_the_first_release_of/)

## CCL

Clozure was a bit active, but rather dormant.

Great news: [**Clozure is back**](https://www.reddit.com/r/Common_Lisp/comments/1c0kq27/clozure_is_back/)

[Clozure CL 1.13 released](https://github.com/Clozure/ccl/releases/tag/v1.13).

## Allegro

[Allegro Common Lisp 11.0 from Franz Inc.](https://franz.com/products/allegro-common-lisp/)

## LispWorks

I didn't spot a patch release (they had a major release in 2022), so let's link to a discussion: [is LispWorks worth it?](https://www.reddit.com/r/Common_Lisp/comments/11979q4/common_lisp_implementations_in_2023/) you might learn some things about LW's feature set.

## ECL

Embeddable, targetting WASM… is it the future?

- [ECL 24.5.10](https://ecl.common-lisp.dev/posts/ECL-24510-release.html)
- [ECL runs Maxima in a browser with WASM](https://mailman3.common-lisp.net/hyperkitty/list/ecl-devel@common-lisp.net/thread/T64S5EMVV6WHDPKWZ3AQHEPO3EQE2K5M/)

## CLASP

CLASP targets C++ on LLVM.

[Release: Clasp v2.5.0](https://github.com/clasp-developers/clasp/releases/tag/2.5.0)

They realeased [Clasp v2.7.0](https://github.com/clasp-developers/clasp/releases/) in January, 2025.

For context:

- [Christian Schafmeister talk - brief update about his "molecular lego" supported by his Lisp compiler](https://www.youtube.com/watch?v=tQgkvghzW0M)
- there's less funding than in the 80s for Common Lisp, but still funding: "CLASP was supported by The Defense Threat Reduction Agency, The National Institutes of Health, The National Science Foundation".

## SICL - the new, portable and modular implementation

The [SICL](https://github.com/robert-strandh/SICL) implementation is very active.

> SICL is a new implementation of Common Lisp. It is intentionally divided into many implementation-independent modules that are written in a totally or near-totally portable way, so as to allow other implementations to incorporate these modules from SICL, rather than having to maintain their own, perhaps implementation-specific versions.

SICL's components are used, for example, in the CLASP implementation.

Related, the [second-climacs](https://github.com/robert-strandh/Second-Climacs) editor shows good activity too. Watch [this demo](https://scymtym.github.io/content/second-climacs-9.mp4), showing on-the-fly code parsing and feedback, it looks pretty cool. Here too, each achievement is extracted into its own component. So the second-climacs' codebase actualy shrinks with time.

If you want to hack on CL, those are good places.


## New implementations

- [A Common Lisp implementation in development in C89 (no compiler so far)](https://www.reddit.com/r/lisp/comments/1gr3z94/a_common_lisp_implementation_in_development/)
  - called ALisp, "breakpoints and stepping work quite well"
- [gsou/LCL: Lua Common Lisp. An implementation of Common Lisp targeting Lua.](https://codeberg.org/gsou/LCL)

## Historical: Medley Interlisp

We can run the Medley Interlisp Lisp machine in a browser O_o The work achieved by this group is phenomenal, look:

- [2023 Medley Interlisp Project Annual Report](https://interlisp.org/project/status/2023medleyannualreport/)

- [2024 Medley Interlisp Project Annual Report](https://interlisp.org/project/status/2023medleyannualreport/)

I suggest to follow `@interlisp@fosstodon.org` on Mastodon.

# Companies and jobs

Yes, some companies still choose Common Lisp today, and some hire with a public job posting.

It's of course the visible top of the iceberg. If you dream of a Lisp
job, I suggest to be active and make yourself visible, you might be
contacted by someone without a proper job announce. This could be for
an open-source project with funding (happened to me), for a
university, etc.

- a new product: [Oracle Aconex Accelerator · "Over 5 years of development and scaling, the entire Conceptual AI linked data platform is built on Common Lisp (SBCL)."](https://graphmetrix.com/Aconex)
- experience report: [the World's Loudest Lisp Program to the Rescue, by Eugene Zaikonnikov ](https://blog.funcall.org/lisp%20psychoacoustics/2024/05/01/worlds-loudest-lisp-program/)
- job: [Common Lisp Developer Job | Backend | Keepit | Kraków](https://nofluffjobs.com/job/common-lisp-developer-keepit-krakow)
- job: (same company, later) [Common Lisp Developer job offer at Keepit](https://careers.keepit.com/jobs/4747213-common-lisp-developer)
- job: [Freelance job posting at Upwork for a Common Lisp Developer/Engineer.](https://www.reddit.com/r/Common_Lisp/comments/11s8rli/freelance_job_posting_at_upwork_for_a_common_lisp/)
- job: [Lisp job: Cognitive Software Engineer - Chelmsford, MA, at Triton Systems](https://tritonsystems.applicantpro.com/jobs/3610597)
- job: [implement the "Convex Hull Covering of Polygonal Scenes" paper.](https://shirakumo.org/jobs)
- job: [Senior Lisp Developer (m/f/d), Software & Applications at DXC Technology, work on SARA (SuperAgent Robotic Application), an automation tool designed to streamline complex and repetitive business processes for major airlines.](https://careers.dxc.com/global/en/job/51516470/Senior-Lisp-Developer-m-f-d-100-remote)
- job: [senior Common Lisp Developer | 3E, Brussels](https://jobs.3e.eu/en/vacature/93847/common-lisp-developer/)

We knew these companies since [awesome-lisp-companies](https://github.com/azzamsa/awesome-lisp-companies/) -it's only a list of companies we know about, nothing offical. Additions welcome.

Discussions on the topic:

- [Anyone using Common Lisp for freelance work?](https://www.reddit.com/r/Common_Lisp/comments/1g9w4cp/anyone_using_common_lisp_for_freelance_work/) where we learn about cool websites made in CL and cool experience reports.
- [Running my 4th Common Lisp script in production© - you can do it too](https://lisp-journey.gitlab.io/blog/running-my-4th-lisp-script-in-production/) aka "you don't need crazily difficult needs to make yourself a favour and use CL instead of Python in your projects"

# Projects

## Editors

Please check out [the Cookbook: editors](https://lispcookbook.github.io/cl-cookbook/editor-support.html) for a list of good editors for Common Lisp. You migth be surprised.

Let's highlight a new editor in town: [Neomacs: Structural Lisp IDE/computing environment ](https://github.com/neomacs-project/neomacs). Mariano integrated it in his moldable web desktop: [Integrating Neomacs into my CLOG-powered desktop](https://github.com/neomacs-project/neomacs/discussions/95).

### About Emacs

- [slime 2.30 · Better I/O performance, macroexpand for macrolet (and more)](https://github.com/slime/slime/releases/tag/v2.30)- [Better Common Lisp highlighting in Emacs](https://www.n16f.net/blog/custom-font-lock-configuration-in-emacs/)
- [Learning Lisp - making sense of xrefs in SLIME](https://dev.to/gabrielshanahan/learning-lisp-making-sense-of-xrefs-in-slime-2b6g)

### About VSCode

- [Experience Report using VS Code + Alive to Write Common Lisp](https://blog.djhaskin.com/blog/experience-report-using-vs-code-alive-to-write-common-lisp/)

### About Lem and Rooms pair programming environment

- [Lem 2.0.0 released](https://github.com/lem-project/lem/releases/tag/v2.0.0)
  - released in May 2023, this version added the SDL2 frontend, adding mouse support, graphic capabilities, and Windows support.
  - it brought the possibility to draw images and shapes at any location on a buffer or window.
  - addition of many base16 color themes (180), by @lukpank.
- [Lem 2.1.0 released](https://github.com/lem-project/lem/releases/tag/v2.1.0), with many new contributors. Lem 2.0 definitely caught the eyes of many developers IMO.
  - this is when Lem got its website: https://lem-project.github.io/
  - @sasanidas worked on supporting other implementations: "ECL and CCL should work fairly well", "ABCL and Clasp are still work in progress, working but with minor bugs.".
  - I added project-aware commands, find-file-recursively
  - @cxxxr added (among everything else) great Lisp mode additions (just look at the release notes and the screenshots)
  - added a sidebar / filer
  - and much more. Just look at the release.
- then came out [Lem 2.2.0](https://github.com/lem-project/lem/releases/tag/v2.2.0)
  - the release notes are less organized ;)
  - added **libvterm** integration
  - this is when I added the interactive git mode.

Unfortunately these latest releases do not ship a readily usable executable. But the installation recipes have been greatly simplified and use Qlot instead of Roswell. There's a one-liner shell command to install Lem on Unixes.

Lem's creator [cxxxr is now on GitHub sponsors](https://github.com/sponsors/cxxxr).

He is also working on [Rooms](https://github.com/rooms-dev/lem-rooms-client), aka Lem on the cloud: it's a Lem-based "**pair programming environment** where you can share your coding sessions". Only the client is open-source, so far at least.

Demo: https://www.youtube.com/watch?v=IMN7feOQOak

Those are the Lem related articles that popped up:

- [Oh no, I started **a Magit-like plugin for the Lem editor**](https://lisp-journey.gitlab.io/blog/oh-no-i-started-a-magit-like-plugin-for-the-lem-editor/)
- [Lem customizable **dashboard**](https://lem-project.github.io/modes/dashboard/)
- [Lem has a subreddit](https://www.reddit.com/r/lem/)
- [**Lem in CLIM** "1.0" · now with mouse events and smoother performance. (August 2024)](https://mastodon.social/@frescosecco/112909105139388650)
- [Lem on the cloud: Powerful web-based Editor with Collaborative Editing](https://www.reddit.com/r/lisp/comments/1cc9yq6/lem_on_the_cloud_powerful_webbased_editor_with/)

{{< rawhtml >}}
<img src="https://lisp-journey.gitlab.io/images/lem-status.png"
  style="max-width: 100%; margin: 2em;" alt="Lem's Legit Git interface." title="An interactive, magit-like Git interface into Lem."></img>
{{< /rawhtml >}}


### About LispWorks

- [LispWorks Plugins by April & May](https://github.com/apr3vau/lw-plugins)
- [Version Control for Lispworks](https://www.reddit.com/r/Common_Lisp/comments/1e5lfcg/version_control_for_lispworks/)

### About the Jetbrains plugin

- [SLT Jetbrains plugin v0.2.0 and 0.2.1 - first version of inspector, macro-expand, basic completion](https://github.com/Enerccio/SLT/releases/tag/v0.2.0)

### About Jupyter

- [Jupyter and Common Lisp: A Powerful Combination for Data Science](https://saturncloud.io/blog/jupyter-and-common-lisp-a-powerful-combination-for-data-science/)

### Other tools

- [CL-REPL now supports multiline editing](https://github.com/digikar99/cl-repl)
  - it also comes as a ready-to-use binary


## Coalton

Coalton is

> the implementation of a static type system beyond Haskell 95. Full multiparameter type classes, functional dependencies, some persistent data structures, type-oriented optimization (including specialization and monomorphization). All integrated and native to CL without external tools.

And used in production for years in the quantum industry. See [quilc](https://github.com/quil-lang/quilc/).

I found Coalton-related projects:

- https://github.com/garlic0x1/coalton-threads
- https://github.com/garlic0x1/coalton-rope
- https://github.com/jbouwman/coalton-lsp

E. Fukamachi added Coalton support for Lem: [https://lem-project.github.io/modes/coalton-lang/](https://lem-project.github.io/modes/coalton-lang/). This adds completion, syntax highlighting, interactive compilation and more inside "coalton-toplevel" forms.


## Package managers

Quicklisp had a one year hiatus, because it relies on one man. It
finally got an update after 1 year: [Quicklisp libraries were updated
2024-10-12](http://blog.quicklisp.org/2024/10/october-2024-quicklisp-dist-update-now.html). Despite
a [call for
collaboration](https://www.reddit.com/r/Common_Lisp/comments/17jukzk/october_2023_quicklisp_dist_update_now_available/?),
we don't really know how we can help.

But Quicklisp isn't the only library manager anymore.

- [introducing ocicl: an experimental modern quicklisp alternative built on tools from the world of containers ](https://www.reddit.com/r/Common_Lisp/comments/13lxhez/ocicl_an_experimental_modern_quicklisp/) may 2023
 - [ocicl no longer depends on the external oras binary](https://www.reddit.com/r/Common_Lisp/comments/1g2qbmp/ocicl_no_longer_depends_on_the_external_oras/)
 - [ocicl: now with AI powers](https://www.reddit.com/r/Common_Lisp/comments/1ckqujh/ocicl_a_common_lisp_system_manager_now_with_ai/)
- [Ultralisp now supports any git repositories as project sources](https://github.com/ultralisp/ultralisp/pull/213)
- [Qlot 1.4.1 - added script for manual installation without Roswell, "qlot install" runs in parallel](https://github.com/fukamachi/qlot/releases/tag/1.4.1)
- [Qlot 1.5.0 - added a REPL interface](https://github.com/fukamachi/qlot?tab=readme-ov-file#repl-experimental)
- [introducing vend: just vendor your dependencies](https://github.com/fosskers/vend)

Also:

- [Announcing deptree | list and archive dependency snapshots of (ASDF-defined) projects](http://blog.funcall.org//lisp/2023/10/23/deptree/)


## Gamedev

The Kandria game was released: https://kandria.com/

- [Trial game engine documentation website and examples](https://shirakumo.github.io/trial/examples)
- [My Lisp physics engine for Trial is finally working! ](https://mastodon.tymoon.eu/@shinmera/111464312394127769)

If you are into game dev, this is a paper you cannot miss: [Kandria: experience report](https://raw.githubusercontent.com/Shinmera/talks/master/els2023-kandria/paper.pdf), presented at the ELS 2023.

Great articles:

- [Gamedev in Lisp. Part 1: ECS and Metalinguistic Abstraction ](https://awkravchuk.itch.io/cl-fast-ecs/devlog/622054/gamedev-in-lisp-part-1-ecs-and-metalinguistic-abstraction)
- [Gamedev in Lisp. Part 2: Dungeons and Interfaces · Wiki · Andrew Kravchuk / cl-fast-ecs · GitLab](https://gitlab.com/lockie/cl-fast-ecs/-/wikis/tutorial-2)

and more:

- [Multiplayer game with Common Lisp + SDL2 on WebAssembly (short demo video)](https://www.youtube.com/watch?v=MsVX_1zg4n4)

I almost forgot the Lisp Game Jams and the new cool little games. For example: [Nano Towers](https://bohonghuang.itch.io/nano-towers)

> a simple tower defense game written in Common Lisp with the EON framework based on Raylib, submitted for the Spring Lisp Game Jam 2024.

![](https://img.itch.zone/aW1hZ2UvMjcyOTE1Mi8xNjI3ODI3NC5wbmc=/347x500/I9n75v.png)

Links to the jams:

- https://itch.io/jam/autumn-lisp-game-jam-2024
- https://itch.io/jam/spring-lisp-game-jam-2023


## GUI

Many solutions exist. Disclaimer: the perfect GUI library doesn't exist. Please see the Cookbook/gui and awesome-cl. Also don't miss the web views available today.

releases:

- [McCLIM 0.9.8 Yule](https://mcclim.common-lisp.dev/posts/McCLIM-098-Yule-release.html)
- [Shirakumo/glfw: An up-to-date Common Lisp bindings library to the most recent GLFW OpenGL context management library ](https://github.com/shirakumo/glfw)
- [nodgui 0.4 released - multithread main loop, auto-completion entry, extended text widget, better image support](https://www.reddit.com/r/Common_Lisp/comments/13qio8q/nodgui_04_released_multithread_main_loop/)
- [nodgui v0.6.0 - Added an SDL frame as an alternative for TK canvas when fast rendering is needed. Both 2D (pixel based) and a 3D rendering (the latter using openGL) are available.](https://emacs.ch/@cage/111856866932486274)
- [Pretty GUIs now: nodgui comes with a pre-installed nice looking theme](https://lisp-journey.gitlab.io/blog/nodgui-now-has-a-nice-looking-theme-by-default/)

As always, we might not highlight the work achieved on existing
libraries that didn't get a proper announce. There are more GUI
libraries for CL.

demos:

- ["cl-gtk4 combines strength of GTK and Common Lisp interactive development." (short screencast)](https://mstdn.io/@veer66/111024762876056271)

## Web

**CLOG** appeared in 2022 and is kicking. Its API has been stable for 4 years.

You know Hacker News, the website, right? [**Hacker News now runs on top of SBCL**](https://www.reddit.com/r/Common_Lisp/comments/1iekqnc/the_production_website_of_hacker_news_now_runs_on/)

> HN runs on top of Arc, the language.
> Arc was implemented on top of Racket (-> MzScheme).
> A new, faster / more efficient, implementation of Arc in SBCL was in the works by a Hacker News site maintainer for some time: called Clarc. Its source code has not been published.
> Since [late september, 2024], the official Hacker News site runs using Clarc and SBCL.

Here's (again) my **new resource for web development** in Common Lisp: [Web Apps in Lisp: Know-how](https://web-apps-in-lisp.github.io/index.html).

Now the links:

- CLOG CLOG 2.0 - Now with a complete Common Lisp **IDE** and **GUI Builder** (with or w/o emacs)
- CLOG OS shell

{{< rawhtml >}}
<img src="https://preview.redd.it/p668uwf3kytc1.png?width=1720&format=png&auto=webp&s=86cbed5246b73eb38bb00afbebee4fa71c30626d"
  style="max-width: 100%; margin: 2em;" alt="CLOG shell" title="CLOG shell"></img>
{{< /rawhtml >}}

- CLOG [CLOG Builder Video Manual Video 5 - Using Projects & Plugins](https://youtu.be/k3V75qWQHNE?feature=shared)
- CLOG debug tools https://github.com/rabbibotton/clog/discussions/361
- CLOG got emacs-like tabs

Projects built with CLOG:

- [mold desktop](https://codeberg.org/mmontone/mold-desktop) - A programmable desktop.
- [CLOG moldable inspector](https://www.reddit.com/r/lisp/comments/1bztxs8/clog_moldable_inspector/), "A moldable Common Lisp object inspector based on CLOG"


{{< rawhtml >}}
<a href="https://codeberg.org/mmontone/mold-desktop">
<img src="https://codeberg.org/mmontone/mold-desktop/media/branch/master/screenshots/screenshot2.webp"
  style="max-width: 100%; margin: 2em;" alt="moldable desktop" title="Web-based moldable desktop built in CLOG."></img>
</a>
{{< /rawhtml >}}


**Weblocks** (continued in the Reblocks project):

- [Height Weblocks (Reblocks) Extensions - now with documentation](https://40ants.com/reblocks/extensions/)
- video: [Weblocks demo: a staff onboarding web app written in Common Lisp (EN Subs)](https://diode.zone/videos/watch/9e379a86-c530-4e9d-b8be-7437b1f7200b)

More:

- [A **WASM** Common Lisp REPL](https://www.reddit.com/r/Common_Lisp/comments/173st0r/a_wasm_common_lisp_repl/)

Articles:

- [Ningle Tutorial 3: Static Files Middleware](https://nmunro.github.io/2025/01/30/ningle-3.html)
- [Towards a **Django-like database admin dashboard** for Common Lisp](https://lisp-journey.gitlab.io/blog/towards-a-database-admin-dashboard-for-common-lisp/)
- [Add **Stripe billing** to your Common Lisp app](https://boogs.life/add-billing-to-your-common-lisp-app/index.html)
- [Building a highly-available web service without a database](https://blog.screenshotbot.io/2024/08/10/building-a-highly-available-web-service-without-a-database/)
- [Building with Parenscript and Preact](https://nickfa.ro/wiki/Building_with_Parenscript_and_Preact)
- [i18n in my Lisp web app with Djula templates and gettext ](https://lisp-journey.gitlab.io/blog/i18n-in-my-lisp-web-app-with-djula-templates-and-gettext/)

videos:

- [How to create new CL library or web-application using the Mystic project generator ](https://www.reddit.com/r/Common_Lisp/comments/11vlsgl/how_to_create_new_cl_library_or_webapplication/)

libraries:

- [JZON hits 1.0 and is at last on the latest QL release: a **correct and safe JSON parser**, packed with features, and also FASTER than the latest JSON library advertised here.](https://www.reddit.com/r/Common_Lisp/comments/117oq9r/jzon_hits_10_and_is_at_last_on_the_latest_ql/)
- [**OpenAPI** client generator](https://github.com/kilianmh/openapi-generator)
- [Pulling portableaserve (Portable AllegroServe) into sharplispers](https://www.reddit.com/r/Common_Lisp/comments/1f6kc5o/pulling_portableaserve_portable_allegroserve_into/)
- [ScrapyCL - The web scraping framework for writing crawlers in Common Lisp ](https://github.com/40ants/scrapycl)
- [Nobody Knows Shoes But Ryo Shoes (A Simple GUI DSL upon CLOG)](https://www.reddit.com/r/Common_Lisp/comments/1h0bx3z/nobody_knows_shoes_but_ryo_shoes_a_simple_gui_dsl/)

The **web views** I mentioned: Electron is a thing, but today we have bindings to webview.h and webUI:

- [Three web views for Common Lisp: build cross platform GUIs with Electron, WebUI or CLOG Frame](https://lisp-journey.gitlab.io/blog/three-web-views-for-common-lisp--cross-platform-guis/)

## Nyxt 4.0 pre-realease - now on Electron

[Nyxt 4.0-pre-release-1](https://nyxt.atlas.engineer/article/release-4.0.0-pre-release-1.org) was published in late 2024.

They are publishing a Flatpak featuring the legacy WebKitGTK port and a new Electron one.

> Electron has better performance and opens the door for macOS and Windows support.


# More libraries

Data structures:

- [fset 1.4 - bug fixes and minor improvements ](https://github.com/slburson/fset/releases)
- [Comparison: FSet vs. Sycamore (and FSet speed-up)](https://scottlburson2.blogspot.com/2024/10/comparison-fset-vs-sycamore.html)

Language extensions, core libraries:

- [Dynamic Let (Common Lisp, MOP)](https://turtleware.eu/posts/Dynamic-Let.html)
- [Equality and Comparison in FSet, CDR8, generic-cl](https://scottlburson2.blogspot.com/2024/09/equality-and-comparison-in-fset.html)
- [generic-cl 1.0](https://github.com/alex-gutev/generic-cl/releases/tag/v1.0)
- [cl-ppcre 2.1.2 - New function: count-matches](https://github.com/edicl/cl-ppcre/releases/tag/v2.1.2)

Iteration:

- [cl-transducers 1.0 - operating over and into plists, hash-tables, CSV](https://github.com/fosskers/cl-transducers/)
- [cl-transducers 1.2.0 - FSet support](https://github.com/fosskers/cl-transducers/releases/tag/v1.2.0)
- [Štar: an iteration construct for Common Lisp](https://www.tfeb.org/fragments/2024/05/15/an-iteration-construct-for-common-lisp/)

Developer tools:

- [BRAKE, an extended breakpoint facility for Common Lisp](http://blog.funcall.org//breaking-the-kernighans-law/)

Threads, actors:

- [Bordeaux-Threads API v2](https://blog.cddr.org/posts/2023-05-27-bordeaux-threads-apiv2/)
- [Sento actor framework 3.0 released - no new features, many API changes: cleanups, obstacles removed, and hopefully a more consistent way of doing things.](https://github.com/mdbergmann/cl-gserver)
- [Sento 3.2 · allows a throughput of almost 2M messages per second](https://github.com/mdbergmann/cl-gserver/releases/tag/3.2.0)

Documentation builders:

- [Add Documentation, Please... with Github Flavoured Markdown · supports cross references and table of contents.](https://github.com/HectareaGalbis/adp-github/tree/main)

Databases:

- [AllegroGraph 8](https://allegrograph.com/blog/)
- [Postmodern v1.33.10 and 1.33.11 released](https://github.com/marijnh/Postmodern/releases/)
- [endatabas/endb v0.2.0-beta.1 · SQL document database with full history (Lisp, Rust)](https://github.com/endatabas/endb/releases/tag/v0.2.0-beta.1)
- [Mito-validate](https://github.com/daninus14/mito-validate)

relational database and first order logic:

- [AP5 - an extension to commonlisp which allows users to "program" in a model of first order logic or a relational database (1990, last update 2024)](https://ap5.com/)


Numerical and scientific:

- [cl-waffe2: (Experimental) Graph and Tensor Abstraction for Deep Learning all in Common Lisp](https://github.com/hikettei/cl-waffe2)
- [numericals` has a slightly better documentation now!](https://digikar99.github.io/numericals/manual/)
- [Common Lisp: Numerical and Scientific Computing - Call for Needs](https://www.reddit.com/r/lisp/comments/18isr9q/common_lisp_numerical_and_scientific_computing/)
- [Lisp Stats 2023 end of year summary](https://lisp-stat.dev/blog/2023/12/29/2023-end-of-year-summary/)
- [More notes on using Maxima in a Lisp runtime](https://mahmoodsheikh36.github.io/post/20230510181916-maxima_in_lisp/)
- [maxima-interface - Simple interface between Common Lisp and Maxima](https://git.sr.ht/~jmbr/maxima-interface)

Plotting:

- [GURAFU: a simple (just usable) plot program for Common Lisp](https://www.reddit.com/r/Common_Lisp/comments/1c31559/gurafu_a_simple_just_usable_plot_program_for/)
- [plotly-user: Use plotly in your browser to explore data from a Common Lisp REPL](https://github.com/ajberkley/plotly-user)
  - "a week-end hack and an excuse to learn CLOG"

Bindings and interfaces:

- [marcoheisig/lang: A library for **seamless multi-language programming**. The currently supported languages are Python and Lisp.](https://github.com/marcoheisig/lang)
- [bike (.NET interface for CL) version 0.14.0. Documentation! .NET-callable classes. ECL support. And more.](https://www.reddit.com/r/lisp/comments/1bpig73/bike_net_interface_for_cl_version_0140/)
- [CL-CXX-JIT: Write C++ functions within Common Lisp](https://github.com/Islam0mar/CL-CXX-JIT)
- [Common Lisp implementation of the Forth 2012 Standard](https://github.com/gmpalter/cl-forth)

Serialization:

- [New binary serialization/deserialization library: cl-binary-store](https://www.reddit.com/r/Common_Lisp/comments/1hz5879/new_binary_serializationdeserialization_library/)
- [cl-naive-store: new version](https://www.reddit.com/r/lisp/comments/19d5j6a/clnaivestore_new_version/)

Date and time:

- [Precise Time - hooking into the operating system to give sub-seconds precise timing information](https://shinmera.github.io/precise-time/)
- [calendar-times - a calendar time library implemented on top of LOCAL-TIME](https://github.com/copyleft/calendar-times)

Utilities:

- [cl-ansi-term: print tables with style, and other script utilities](https://lisp-journey.gitlab.io/blog/cl-ansi-term--print-tables-with-style-and-other-script-utilities/)
- [command-line-args - Turn your Common Lisp function into a command which you can use from the command line.](https://git.sr.ht/~whereiseveryone/command-line-args) (similar to [defmain](https://github.com/40ants/defmain))
- [file-finder: Rapid file search and inspection](https://github.com/lisp-maintainers/file-finder/)
- [Consfigurator 1.4.1 released, including new support for FreeBSD](https://www.chiark.greenend.org.uk/pipermail/sgo-software-announce/2024/000089.html)

Bindings and interfaces to other software:

- [cl-git 2.0 - an interface to the C library libgit2. The API is an almost complete exposure of the underlying library](https://cl-git.russellsim.org/changelog.html#id2)
- [cl-telegram-bot - Telegram Bot API (now with documentation)](https://40ants.com/cl-telegram-bot/)
- [claw-raylib - Fully auto-generated Common Lisp bindings to Raylib and Raygui using claw and cffi-object](https://github.com/bohonghuang/claw-raylib)

Networking:

- [aether · Distributed system emulation in Common Lisp ](https://github.com/dtqec/aether) [ELS talk](https://www.youtube.com/watch?v=CGt2rDIhNro)

## Scripting

(I love what's being done here)

- [ruricolist/kiln: Infrastructure for scripting in Common Lisp to make Lisp scripting efficient and ergonomic.](https://github.com/ruricolist/kiln)
- [CIEL Is an Extended Lisp, Hacker News](https://news.ycombinator.com/item?id=41401415)
- [unix-in-lisp](https://github.com/neomacs-project/unix-in-lisp) -  Mount Unix system into Common Lisp image.


## Software releases

- [OpusModus 3.0, first Windows version released](https://i.redd.it/ffb8gu6zobta1.png)
  - [HN discussion](https://news.ycombinator.com/item?id=38188788)
- [tamurashingo/reddit1.0: Refactored **old reddit source code** (with recent commits and a Docker setup)](https://github.com/tamurashingo/reddit1.0/)
- [Release 1.0.0 · calm - Canvas and Lisp magic](https://github.com/VitoVan/calm/releases/tag/1.0.0)
- [Lisa: A production-ready **expert-system** shell, written in thoroughly modern Common Lisp.](https://github.com/youngde811/Lisa)
- [todolist-cl 3.0 - a todolist web UI, written in Common Lisp, with Hunchentoot, Spinneret templates, Mito ORM.](https://github.com/KikyTokamuro/todolist-cl) (by a CL newcomer)
- [iescrypt: a tool to encrypt and/or sign files. Lisp and C versions.](https://codeberg.org/glv/iescrypt)

# Other articles

- [**A Tour of the Lisps**](https://www.fosskers.ca/en/blog/rounds-of-lisp)
- [Why I chose Common Lisp](https://blog.djhaskin.com/blog/why-i-chose-common-lisp/)
- [practicing statistics with Common Lisp (Jupyter notebook)](https://github.com/Lisp-Stat/IPS9/blob/master/notebooks/Part%20I/Chapter%201%20Looking%20at%20Data.ipynb)
- [Full Common Lisp (sbcl) and a CLOG dev environment on/from an Android device](https://www.reddit.com/r/lisp/comments/18t09pf/full_common_lisp_sbcl_and_a_clog_dev_environment/)

# Videos

Demos:

- [AudioVisual in CommonLisp (cl-collider, cl-visual) (screencast)](https://www.youtube.com/watch?v=gdjkSkRFcr4)
- [Cheesy trailer for recent kons-9 **3D graphics** features.](https://youtu.be/liaLgaTOpYE)
- [Drum N Bass in CommonLisp](https://www.youtube.com/watch?v=jS84KmkkNkU)
- [Drum and Bass with a Counterpoint - How to Tutorial - Opusmodus](https://www.youtube.com/watch?v=ah7jfWd5m9A)
- [How Lisp is designing **Nanotechnology** (Developer Voices, with Prof. Christian Schafmeister) (Youtube)](https://www.youtube.com/watch?v=fytGL8vzGeQ)
- [How to Package Common Lisp Software for Linux? EN Subs (alien-works-delivery, linux-packaging)](https://www.youtube.com/watch?v=lGS4sr6AzKw)
- [Melodic Techno - How to Tutorial - **Opusmodus**](https://www.youtube.com/watch?v=G7KwJp9UAd8)
- [The Opusmodus Studio - Everything I didn't know I needed - Subject Sound (YouTube) ](https://www.youtube.com/watch?v=3lruQIx1Rds)
- [Welcome to Opusmodus (Youtube)](https://www.youtube.com/watch?v=BMoHE96rh7w)
- [Identicons and Clozure Common Lisp, by R. Matthew Emerson](https://www.youtube.com/watch?v=8QCbp7ct2w0)


Web:

- [Dynamic Page With **HTMX** and Common Lisp](https://www.reddit.com/r/Common_Lisp/comments/18wkr41/dynamic_page_with_htmx_and_common_lisp/)
- [Common Lisp web development tutorial: **how to build a web app in Lisp** · part 2](https://www.youtube.com/watch?v=EFRVHmOCE7Q) [part 1](https://www.youtube.com/watch?v=h_noB1sI_e8)
- [Missing **Clack** Guide! Build a Web Application in Common Lisp Like a Pro! ](https://youtu.be/sYgA7Nt3rvo)
- [**URL shortener** using Hunchentoot and BKNR](https://youtu.be/smTG3niedsg)
- [web page graphics with **lisp-stat, data-frame and vega plot**](https://youtu.be/ZGgvDj00SZE)


More from the ELS (see their Youtube channel):

- [An Introduction to **Array Programming** in Petalisp, by Marco Heisig, ELS 2024 ](https://zenodo.org/records/11062314)
- [Lightning Talk: **Julia** Functions, Now in Lisp](https://yewtu.be/watch?v=7IpnmXnO1RU)
- [Lightning Talk: Valtan: Write Webapp Everything in Common Lisp: European Lisp Symposium](https://youtu.be/k6zkOTdTiF8?si=eT3yGHJ_ef4Suz7s)
- [ELS2023: Common Lisp Foundation](https://youtu.be/u-jdqpZFlkA)

Learning:

- [I published 17 videos about Common Lisp macros - **learn Lisp with a code-first tutorial**](https://lisp-journey.gitlab.io/blog/17-new-videos-on-common-lisp-macros/) [comments](https://www.reddit.com/r/lisp/comments/16jh8pk/i_published_17_videos_about_common_lisp_macros/)
- [Common Lisp Study Group: experiments with CFFI](https://www.reddit.com/r/common_lisp/comments/1c3pre6/common_lisp_study_group_experiments_with_cffi/)
- [CLOS: Introduction and usage of defclass](https://youtu.be/cssHR_MLwNQ)
- [Nekoma Talks #19 - Common Lisp from **a Clojurian perspective** Part 2 (YouTube)](https://www.youtube.com/watch?v=5-Ug1gitygQ), [part 1](https://www.youtube.com/watch?v=NgI14YHVI-I)
- [Review of **8 Common Lisp IDEs**! Which one to choose? (EN Subs)](https://youtu.be/eTgDaMREKT4)


Aaaand that's it for the tour of the last couple years. Tell me if I missed something. I'll keep updating this post for a few days.

Happy lisping and show us what you build!
