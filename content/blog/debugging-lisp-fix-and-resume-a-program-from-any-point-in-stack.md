---
title: "Debugging Lisp: fix and resume a program from any point in stack 🎥"
date: 2022-12-06T12:47:04+01:00
tags: ["debugging", "tutorial",]
draft: false
---

You are doing god's work on a time-intensive computation, but your final step errors out :S Are you doomed to start everything from zero, and wait again for this long process? No! Find out.

I show this with Emacs and Slime, then with the [Lem editor](https://github.com/lem-project/lem/) (ready-to-use for CL, works with many more languages thanks to its LSP client).

(This video is so cool :D Sound on)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jBBS4FeY7XM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

We use the built-in Common Lisp interactive debugger that lets us restart one precise frame from the call stack. Once you get the debugger:

- press "v" on a frame to go to the corresponding source file
- fix the buggy function, compile it again (C-c C-c in Slime)
- come back to the debugger, place the point on the frame you want to try again, press "r" (sldb-restart-frame, see your editor's menu)
- see everything succeed. You did *not* restart everything from zero.
- if you don't see some function calls and variables in the debugger, compile your code with max debug settings (`C-u C-c C-c` in Slime).


For more:

- https://lispcookbook.github.io/cl-cookbook/debugging.html - see how to break and step, how to trace function calls, etc.

Other videos:

- [How to create a full-featured Common Lisp project](https://www.youtube.com/watch?v=XFc513MJjos) with [my project generator](https://github.com/vindarel/cl-cookieproject).
- [How to call a REST API in Common Lisp - example with the GitHub API](https://www.youtube.com/watch?v=TAtwcBh1QLg), sending HTTP requests, parsing JSON, building a binary.

Hope you learned something!

---

![Debugging a complex stacktrace in Python VS Common Lisp (unknown author)](/images/debugging-python-VS-lisp.png)
