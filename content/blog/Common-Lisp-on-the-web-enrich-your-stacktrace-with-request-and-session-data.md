---
title: "Common Lisp on the web: enrich your stacktrace with request and session data"
date: 2023-10-13T16:51:07+02:00
draft: false
tags: ["web",]
---

A short post to show the usefulness of [Hunchentoot-errors](https://github.com/mmontone/hunchentoot-errors) and to thank Mariano again.

This library adds the current request and session data to your stacktrace, either in the REPL (base case) or in the browser.

TLDR;

Use it like this:

~~~lisp
;; (ql:quickload "hunchentoot-errors)
;;
;; We also use easy-routes: (ql:quickload "easy-routes")

(defclass acceptor (easy-routes:easy-routes-acceptor hunchentoot-errors:errors-acceptor)
  ()
  (:documentation "Our Hunchentoot acceptor that uses easy-routes and hunchentoot-errors, for easier route definition and enhanced stacktraces with request and session data."))
~~~

then `(make-instance 'acceptor :port 4242)`.


# Base case

Imagine you have a bug in your route:

~~~lisp
(easy-routes:defroute route-card-page ("/card/:slug" :method :GET :decorators ((@check-roles admin-role)))
    (&get debug)
  (error "oh no"))
~~~

When you access `localhost:4242/card/100-common-lisp-recipes`, you will see this in the REPL:

```
[2023-10-13 16:48:21 [ERROR]] oh no
Backtrace for: #<SB-THREAD:THREAD "hunchentoot-worker-127.0.0.1:53896" RUNNING {10019A21A3}>
0: (TRIVIAL-BACKTRACE:PRINT-BACKTRACE-TO-STREAM #<SB-IMPL::CHARACTER-STRING-OSTREAM {1006E9ED43}>)
1: (HUNCHENTOOT::GET-BACKTRACE)
2: ((FLET "H0" :IN HUNCHENTOOT:HANDLE-REQUEST) #<SIMPLE-ERROR "oh no" {1006E9EBE3}>)
3: (SB-KERNEL::%SIGNAL #<SIMPLE-ERROR "oh no" {1006E9EBE3}>)
4: (ERROR "oh no")
5: (MYWEBAPP/WEB::ROUTE-CARD-PAGE "100-common-lisp-recipes")
6: ((:METHOD HUNCHENTOOT:ACCEPTOR-DISPATCH-REQUEST (EASY-ROUTES:EASY-ROUTES-ACCEPTOR T)) #<MYWEBAPP/WEB::ACCEPTOR (host *, port 4242)> #<HUNCHENTOOT:REQUEST {1006C55F33}>) [fast-method]
7: ((:METHOD HUNCHENTOOT:HANDLE-REQUEST (HUNCHENTOOT:ACCEPTOR HUNCHENTOOT:REQUEST)) #<MYWEBAPP/WEB::ACCEPTOR (host *, port 4242)> #<HUNCHENTOOT:REQUEST {1006C55F33}>) [fast-method]
[…]
```

And, by default, you see a basic error message in the browser:

{{< rawhtml >}}
<img src="/images/hunchentoot-error.png" style="max-width: 100%"/>
{{< /rawhtml >}}

# Show errors

Set this:

~~~lisp
(setf hunchentoot:*show-lisp-errors-p* t)
~~~

Now you can see a backtrace in the browser window, which is of course
super useful during development:

{{< rawhtml >}}
<img src="/images/hunchentoot-show-error.png" style="max-width: 100%"/>
{{< /rawhtml >}}

BTW, if you unset this one:

~~~lisp
(setf hunchentoot:*show-lisp-backtraces-p* nil)  ;; t by default
~~~

You will see the error message, but not the backtrace:

{{< rawhtml >}}
<img src="/images/hunchentoot-error-no-backtrace.png" style="max-width: 100%"/>
{{< /rawhtml >}}


And I remind you that if you set `*catch-errors-p*` to nil, you'll get the debugger inside your IDE (Hunchentoot will *not* catch the errors, and will pass it to you).

# Now with request and session data

Now create your server with our new acceptor, inheriting hunchentoot-errors.

You'll see the current request and session paramaters both in the REPL:

```
[…]
19: (SB-THREAD::NEW-LISP-THREAD-TRAMPOLINE #<SB-THREAD:THREAD "hunchentoot-worker-127.0.0.1:48756" RUNNING {100AAAFC43}> NIL #<CLOSURE (LAMBDA NIL :IN BORDEAUX-THREADS::BINDING-DEFAULT-SPECIALS) {100AAAFBEB}> NIL)
20: ("foreign function: call_into_lisp")
21: ("foreign function: new_thread_trampoline")

HTTP REQUEST:
  uri: /card/100-common-lisp-recipes
  method: GET
  headers:
    HOST: localhost:4242
    USER-AGENT: Mozilla/5.0 (X11; Linux x86_64; rv:103.0) Gecko/20100101 Firefox/103.0
    ACCEPT: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
    ACCEPT-LANGUAGE: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
    ACCEPT-ENCODING: gzip, deflate, br
    DNT: 1
    CONNECTION: keep-alive
    COOKIE: "..."
    UPGRADE-INSECURE-REQUESTS: 1
    SEC-FETCH-DEST: document
    SEC-FETCH-MODE: navigate
    SEC-FETCH-SITE: none
    SEC-FETCH-USER: ?1

SESSION:
  :USER: #<MYWEBAPP.MODELS:USER {100EA8C753}>

127.0.0.1 - [2023-10-13 17:32:18] "GET /card/100-common-lisp-recipes HTTP/1.1" 500 5203 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:103.0) Gecko/20100101 Firefox/103.0"
```

and in the browser:

{{< rawhtml >}}
<img src="/images/hunchentoot-error-with-request.png" style="max-width: 100%"/>
{{< /rawhtml >}}

(notice the `#<USER {…}>` at the bottom? You'll need a commit from today to see it, instead of `#` only)

# Final words

These Hunchentoot variables were kinda explained on the [Cookbook/web.html](https://lispcookbook.github.io/cl-cookbook/web.html#hunchentoot-5), I'll augment that.

Clack users can use the `clack-errors` midleware.

{{< rawhtml >}}
<img src="https://vindarel.github.io/cl-cookbook/assets/clack-errors.png" style="max-width: 100%"/>
{{< /rawhtml >}}

Who wants to send a PR for colourful stacktraces?


- [Common Lisp Read the Docs: Hunchentoot](https://common-lisp-libraries.readthedocs.io/hunchentoot/)
- [awesome-cl#web](https://github.com/CodyReichert/awesome-cl#web-frameworks)
