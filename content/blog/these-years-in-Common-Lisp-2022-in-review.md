---
title: "These Years in Common Lisp: 2022 in review"
date: 2023-01-09T19:54:29+01:00
tags: ["libraries",]
draft: false
---

And 2022 is over. The Common Lisp language and environment are solid
and stable, yet evolve. Implementations, go-to libraries, best
practices, communities evolve. We don't need a "State of the
Ecosystem" every two weeks but still, what happened and what did you
miss in 2022?

This is my pick of the most exciting, fascinating, interesting or just
cool projects, tools, libraries and articles that popped-up during
that time (with a few exceptions that appeared in late 2021).

This overview is not a ["State of the CL ecosystem"](https://lisp-journey.gitlab.io/blog/state-of-the-common-lisp-ecosystem-2020/) ([HN comments (133)](https://news.ycombinator.com/item?id=26065511)) that I did in
2020, for which you can find complementary comments on HN.

>  I think this article (of sorts) is definitely helpful for onlookers to Common Lisp, but doesn't provide the full "story" or "feel" of Common Lisp, and I want to offer to HN my own perspective.

And, suffice to say, I tried to talk about the most important things,
but this article (of sorts) is by no means a compilation of *all* new
CL projects or all the articles published on the internet. Look on
Reddit, Quicklisp releases, Github, and my favourite resources:

- [Awesome-cl](https://github.com/CodyReichert/awesome-cl) - a curated list of libraries (there might be more than you think)
- [the CL Cookbook](https://lispcookbook.github.io/cl-cookbook/)

If I had to pick 3 achievements they would be:

- SBCL developments: **SBCL is now callable as a shared library**. See below in "Implementations".
- **a new 3D graphics project: Kons-9**: "The idea would be to develop a system along the lines of Blender/Maya/Houdini, but oriented towards the strengths of Common Lisp". And the project progresses at a good pace.
- **CLOG, the Common Lisp Omnificent GUI**. It's like a GUI framework
  to create web apps. Based on websockets, it offers a light
  abstraction to create fully-dynamic web applications, in Common
  Lisp. It has lots of demos to create websites, web apps, games, and
  it ships a complete editor. For development, we can connect our Lisp
  REPL to the browser, and see changes on the fly. The author had a
  similar commercial product written in Ada, discovered Common Lisp,
  and is now super active on this project.

Let's go for more.


Thanks to @k1d77a, @Hexstream, @digikar and @stylewarning for their feedback.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Documentation](#documentation)
- [Implementations](#implementations)
- [Jobs](#jobs)
- [Projects](#projects)
    - [Language libraries](#language-libraries)
    - [Editors, online editors, REPLs, plugins](#editors-online-editors-repls-plugins)
    - [Concurrency](#concurrency)
    - [Databases](#databases)
    - [Delivery tools](#delivery-tools)
    - [Games](#games)
    - [Graphics, GUIs](#graphics-guis)
        - [Kons-9, a new 3D graphics project](#kons-9-a-new-3d-graphics-project)
    - [Interfaces with other languages](#interfaces-with-other-languages)
    - [Numerical and scientific](#numerical-and-scientific)
    - [Web](#web)
        - [CLOG](#clog)
    - [New releases](#new-releases)
    - [(re) discoveries](#re-discoveries)
- [Articles](#articles)
    - [Graphics](#graphics)
    - [Tooling](#tooling)
    - [Around the language](#around-the-language)
    - [Web related](#web-related)
    - [Other articles](#other-articles)
- [Screencasts and podcasts](#screencasts-and-podcasts)
- [Other discussions](#other-discussions)
    - [Community](#community)
    - [Learning Lisp](#learning-lisp)
    - [Common Lisp VS …](#common-lisp-vs-)

<!-- markdown-toc end -->



# Documentation

A newcomer to Lisp came, asked a question, and suddenly he created a super useful rendering of the specification. Check it out!

- 🚀 [Common Lisp CommunitySpec (CLCS)](https://cl-community-spec.github.io/pages/index.html) -  a rendition of the Common Lisp ANSI Specification draft.
  - Github: https://github.com/fonol/cl-community-spec
  - It is readable, it has syntax highlighting for code snippets, an interactive search bar, it looks modern, it is open.

But that's not all, he also started work on a new Common Lisp editor, built in Rust and Tauri, see below.

We continue to enrich the Common Lisp Cookbook. You are welcome to join, since documention is best built by newcomers for newcomers.

- [New CL Cookbook EPUB and PDF release - mainly readability and file format improvements - thanks to the 13 contributors](https://github.com/LispCookbook/cl-cookbook/releases/tag/2022-09-14)

A resurrected project:

- 🚀 [Learn Lisp the Hard Way is back online](https://llthw.common-lisp.dev/)

Also:

- [the revamped Common Lisp Document Repository (CDR) site](https://cdr.common-lisp.dev/), "a repository of documents that are of interest to the Common Lisp community. The most important property of a CDR document is that it will never change"
  - [comments](https://www.reddit.com/r/lisp/comments/s9r57l/via_marco_antoniottis_blog_the_revamped_common/)
- [I am creating a Common Lisp video course on Udemy 🎥](https://lisp-journey.gitlab.io/blog/i-am-creating-a-common-lisp-video-course-on-udemy/)
  - read more about my motivation and follow the project on Github: https://github.com/vindarel/common-lisp-course-in-videos/
  - the course has some free videos. If you are a student, drop me a line for a free link.
  - [direct link to the course](https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358).
  - Thanks for your support!

# Implementations

We saw achievements in at least ~~7~~ 8 implementations.

- [SBCL continues to ship monthly](http://www.sbcl.org/all-news.html). In 2022:
  - 🚀 SBCL is now **callable as a shared library**. See sbcl-librarian below.
  - 🚀 [sb-simd](https://www.sbcl.org/manual/index.html#sb_002dsimd) - **SIMD** programming in SBCL.
  - **core compression** uses zstd instead of zip: compression is about 4 times faster, decompression about two times, compression saves ±10% of size.
  - `trace` now supports tracing macro functions, compiler-macro functions, individual methods and local functions (flet and labels) (SBCL 2.2.5)
  - the SBCL repository reached 20,000 commits.
  - [Prebuilt SBCL binary for Android (Termux)](https://github.com/bohonghuang/sbcl-termux-build) (unofficial)
- [Clasp 2.0.0 released](https://github.com/clasp-developers/clasp/releases/tag/2.0.0)
  - This is Common Lisp on LLVM.
  - [Christian Schafmeister talk - brief update about his "molecular lego" supported by his Lisp compiler](https://www.youtube.com/watch?v=tQgkvghzW0M)
  - there's less funding than in the 80s, but still funding: "CLASP was supported by The Defense Threat Reduction Agency, The National Institutes of Health, The National Science Foundation".
- ECL:
  - [LQML: a lightweight ECL binding to QML (both Qt5 and Qt6) derived from EQL5](https://www.reddit.com/r/lisp/comments/sqrdio/lqml_a_lightweight_ecl_binding_to_qml_both_qt5/)
    - tested on the following platforms: Linux, macOS, android, iOS.
  - [ECL targetting WASM via Emscripten - preliminary support](https://www.reddit.com/r/lisp/comments/ys7jpl/ecl_targetting_wasm_via_emscripten_preliminary/)
- [LispWorks Personal Edition updated to version 8.0.1](http://www.lispworks.com/news/news41.html), incl. *native Apple Silicon version*
  - [ additional important LispWorks 8 patch for macOS 12.6](http://www.lispworks.com/downloads/patch-selection.html#lw80monterey)
- [Major New release of Allegro Common Lisp Express Edition for 10.1](https://www.reddit.com/r/Common_Lisp/comments/tb5ehb/major_new_release_of_allegro_common_lisp_express/)
  - Browser-based IDE for Linux and macOS
    - no syntax highlighting for the editor though :S
  - Applications built using Common Graphics can use browsers for delivery. Firefox, Chrome, Safari, Edge and many other browsers are supported.
  - New platforms: aarch64, x86_64
  - download: https://franz.com/downloads/clp/survey
- [ABCL 1.9.0](https://abcl.org/release-notes-1.9.0.shtml) was released in May.
  - "ABCL 1.9.0 has been best tested on the openjdk8, openjdk11, and openjdk17 runtimes. It will run other places but those are the best supported."
- [GNU Common Lisp 2.6.13 released](https://www.reddit.com/r/lisp/comments/zqy697/gnu_common_lisp_2613_is_released/), after 8 years.

New implementation! It's 2022 and people start new CL implementations.

- [NPT](https://github.com/nptcl/npt) - an implementation of ANSI Common Lisp in C.
  - [comments](https://www.reddit.com/r/Common_Lisp/comments/uiewwn/npt_an_implementation_of_ansi_common_lisp_in_c/)

See also:

- [LCL, Lua Common Lisp](https://codeberg.org/gsou/LCL) - The goal of this project is to provide an implementation of Common Lisp that can be used wherever an unmodified Lua VM is running.
  - *not* a complete implementation.

They are doing great work to revive a Lisp machine:

- [2022 Medley Interlisp Annual Report](https://interlisp.org/news/2022medleyannualreport/)

> Medley Interlisp is a project aiming to restore the Interlisp-D software environment of the Lisp Machines Xerox produced since the early 1980s, and rehost it on modern operating systems and computers. It's unique in the retrocomputing space in that many of the original designers and implementors of major parts of the system are participating in the effort.

Paolo Amoroso [blog post: my encounter with Medley Interlisp](https://journal.paoloamoroso.com/my-encounter-with-medley-interlisp).

# Jobs

I won't list expired job announces, but this year Lispers could apply for jobs in: web development([WebCheckout](https://webcheckout.net/), freelance announces), cloud service providers ([Keepit](https://www.keepit.com/)), big-data analysis ([Ravenpack](https://www.ravenpack.com/), and chances are they are still hiring)), quantum computing ([HLR Laboratories](https://www.hrl.com/)), AI ([Mind AI](https://mind.ai/), [SRI International](https://www.sri.com/)), real-time data aggregration and alerting engines for energy systems ([3E](https://3e.eu/)); for a startup building autism tech (and using CLOG already); there have been a job seeking to rewrite a Python backend to Common Lisp ([RIFFIT](https://www.riffitnow.com/)); there have been some bounties; etc.

Prior Lisp experience was not 100% necessary. There were openings for junior and senior levels, remote and not remote (Australia for "a big corp", U.S., Spain, Ukraine…).

Comes a question:

- [Any Ideas for strategies to find jobs with lisp?](https://www.reddit.com/r/lisp/comments/vvszte/any_ideas_for_strategies_to_find_jobs_with_lisp/)

I remind the reader that most Lisp jobs do *not* have a public job posting, instead candidates are often found organically on the community channels: IRC, Twitter, Discord, Reddit… or teams simply train their new developer.

In 2022 we added a few companies to the ongoing, non-official list on [awesome-lisp-companies](https://github.com/azzamsa/awesome-lisp-companies/). If your company uses Common Lisp, feel free to tell us on an issue or in the comments!

For example, [Feetr.io](https://feetr.io/) "is entirely Lisp".

> Lisp was a conscious decision because it allows a small team to be incredibly productive, plus the fact that it's a live image allows you to connect to it over the internet and poke and prod the current state, which has really allowed a much clearer understanding of the data.

They post SLY screenshots on their Twitter^^

[Evacsound](https://evacsound.com/) ([HN](https://news.ycombinator.com/item?id=32178351)):

> We're using CL in prod for an embedded system for some years now, fairly smooth sailing. It started out as an MVP/prototype so implementation was of no concern, then gained enough velocity and market interest that a rewrite was infeasible. We re-train talent on the job instead.

[Pandorabots](https://home.pandorabots.com/), or [barefootnetworks](https://www.barefootnetworks.com), designing the Intel Tofino programmable switches, and more.



# Projects

- [Ultralisp now supports tags. We can browse a list of projects under a tag.](https://ultralisp.org/tags/api/)
  - Ultralisp is a Quicklisp distribution that ships every five minutes.
  - see also [CLPM](https://www.timmons.dev/posts/clpm-040-released.html) for a new package manager.

## Language libraries

- [Typo: A portable type inference library for Common Lisp](https://github.com/marcoheisig/Typo/), by Marco Heisig.
- Testing:
    - [fiveam-matchers](https://github.com/tdrhq/fiveam-matchers) - An extensible, composable matchers library for FiveAM.
    - [testiere](https://www.reddit.com/r/Common_Lisp/comments/z4hfl9/testiere_tddlike_dev_for_common_lisp_tests_are/) - TDD-like dev for Common Lisp. Tests are included at the top of a defun/t form. When you recompile your functions interactively, the tests are run.
    - [melisgl/try test framework.](https://github.com/melisgl/try/) - "it is what we get if we make tests functions and build a test framework on top of the condition system."
    - [journal](https://github.com/melisgl/journal) - A Common Lisp library for logging, tracing, testing and persistence
    - [40ants-critic](https://github.com/40ants/40ants-critic) - a wrapper around LISP-CRITIC which provides a better interface to analyze ASDF systems and a command-line interface.
- [CLEDE - the Common Lisp Emacs Development Environment ](https://www.reddit.com/r/lisp/comments/x2i3lb/clede_the_common_lisp_emacs_development/)
  - "The idea is to supply features that other language with and static analyzer have, like refactoring and code generation."
- [easy-macros](https://github.com/tdrhq/easy-macros) - An easy way to write 90% of your macros.
- [Quicklisp doctor](https://github.com/bigos/quicklisp-doctor) - a program that examines the quicklisp installation.
- [more-cffi: Additional helper macros for the cffi project](https://github.com/Hectarea1996/more-cffi/)

## Editors, online editors, REPLs, plugins

- 🚀 [Parrot](https://github.com/fonol/parrot/) -  A cross-platform Common Lisp editor.
  - built in **Rust** with **Tauri**, CodeMirror, the Slynk server.
  - "A hobby project started in Summer 2022. It aims to be an editor for Common Lisp (SBCL), that mostly works out of the box."
  - in development, untested on Linux and Mac.
- [Alive LSP for VSCode v0.1.9 · Add initial step debugger support](https://github.com/nobody-famous/alive-lsp/releases/tag/v0.1.9)
- [Common Lisp at Riju](https://riju.codes/commonlisp), a fast online playground for every programming language.
- [Codewars (code training platform) now has Common Lisp (SBCL 2.0.9)](https://docs.codewars.com/languages/commonlisp/)
- [Mobile app "cl-repl" (LQML) to replace "CL REPL" (EQL5)](https://www.reddit.com/r/lisp/comments/zlrkcq/mobile_app_clrepl_lqml_to_replace_cl_repl_eql5/)
- ☆ [slime-star](https://github.com/mmontone/slime-star) - SLIME configuration with some extensions pre-installed.
  - a Lisp System Browser
  - SLIME Doc Contribs
  - Quicklisp Systems browsers
  - Quicksearch utility
  - [Slime breakpoints](https://github.com/mmontone/slime-breakpoints) - Inspect objects from their printed representation in output streams.
  - custom utilities and menus.
- [parachute-browser: A lightweight UI for using the Parachute testing framework in LispWorks](https://github.com/julian-baldwin/parachute-browser)

New releases:

- [Lem editor 1.10.0: lsp-mode by default, multiple cursors, sql mode, and more.](https://github.com/lem-project/lem/releases/tag/v1.10.0)
  - Lem is a general purpose editor written in Common Lisp. It works for many languages thanks to its LSP client.

<img src="https://raw.githubusercontent.com/fonol/parrot/main/devlog/load-file-debug-dialog.jpg" style="max-width: 800px"/>


## Concurrency

- 🚀 [New version of the Sento Actor Framework released](https://github.com/mdbergmann/cl-gserver)  with a few new goodies in future handling. Nicer syntax and futures can now be mapped.
  - in v2.2.0: stashing and replay of messages.
  - in v1.12: "Shutdown and stop of actor, actor context and actor system can now wait for a full shutdown/stop of all actors to really have a clean system shutdown."
- [Writing distributed apps with cl-etcd](https://www.reddit.com/r/Common_Lisp/comments/vsbfbp/writing_distributed_apps_with_cletcd/)

See also [lisp-actors](https://github.com/dbmcclain/Lisp-Actors/), which also does networking. It looks like more of a research project, as it doesn't have unit-tests nor documentation, but it was used for the (stopped) Emotiq blockchain.

Discussions:

- [Concurrency: Common Lisp vs Clojure](https://www.reddit.com/r/lisp/comments/x3425f/concurrency_common_lisp_vs_clojure/)
- [Moving from the BEAM to Common Lisp: What are my concurrency options?](https://www.reddit.com/r/lisp/comments/tna6zo/moving_from_the_beam_to_common_lisp_what_are_my/)

## Databases

- [DuckDB Lisp bindings](https://github.com/ak-coram/cl-duckdb)
- [ndbapi: Common Lisp bindings to the C++ NDB API of RonDB](https://www.reddit.com/r/Common_Lisp/comments/yja88u/ndbapi_common_lisp_bindings_to_the_c_ndb_api_of/)
- [CLSQL released under a non-restrictive license](https://www.reddit.com/r/Common_Lisp/comments/yh60n2/clsql_released_under_a_nonrestrictive_license/)
- [Document Store/DB Implemented in Common Lisp](https://zaries.wordpress.com/2022/05/31/cl-naive-store/)

More choices: [awesome-cl#databases](https://github.com/CodyReichert/awesome-cl#database).

## Delivery tools

There has been outstanding work done there. It is also great to see the different entities working on this. That includes SBCL developers, Doug Katzman of Google, and people at HRL Laboratories (also responsible of [Coalton](https://coalton-lang.github.io/), Haskell-like on top of CL).

> Have you ever wanted to call into your Lisp library from C? Have you ever written your nice scientific application in Lisp, only to be requested by people to rewrite it in Python, so they can use its functionality? Or, maybe you've written an RPC or pipes library to coordinate different programming languages, running things in different processes and passing messages around to simulate foreign function calls.

> […]  If you prefer using SBCL, you can now join in on the cross-language programming frenzy too.

- 🎉 [sbcl-librarian](https://github.com/quil-lang/sbcl-librarian) - An opinionated interface for creating C- and Python-compatible shared libraries in Common Lisp with SBCL. Requires SBCL version >2.1.10.
  - [introductory blogpost](https://mstmetent.blogspot.com/2022/04/using-lisp-libraries-from-other.html): Using Lisp libraries from other programming languages - now with SBCL.
  - [HN comments (67)](https://news.ycombinator.com/item?id=31054796)
- 🚀 [alien-works-delivery](https://github.com/borodust/alien-works-delivery) - WIP system for delivering Common Lisp applications as executable bundles. For now it only supports AppImage format for Linux and MSIX for Windows, but .APK for Android and later MacOSX and iOS bundle formats are planned too.
- [Support for compiling Common Lisp code using bazel.io](https://github.com/qitab/bazelisp), by the CL team at Google.

## Games

Kandria launches on Steam on the 11th of January, in two days!

- [2022 for Kandria in review](https://reader.tymoon.eu/article/419)
  - https://kandria.com/
  - [HN comments (32)](https://news.ycombinator.com/item?id=32043026)
  - [They Successfully completed their Kickstarter](https://www.kickstarter.com/projects/shinmera/kandria/posts/3539140)
  - [They received the Pro Helvetia grant](https://courier.tymoon.eu/view/OENrrf0y2d8V7fx8WudSlUlERXpNQT09)
  - [An Overview of Kandria's Development with Lisp](https://reader.tymoon.eu/article/413)

- [Jettatura - old school RPG game released on Steam this October - built in Common Lisp ("a decade in the making") ](https://store.steampowered.com/app/2023440/Jettatura/)
- [cl-defender](https://github.com/aymanosman/cl-defender) - Toy Game using Common Lisp and Raylib.

🎥 [Kandria trailer](https://www.youtube.com/watch?v=usc0Znm-gbA).


<!-- <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/usc0Znm-gbA" title="Kandria trailer" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> -->

## Graphics, GUIs

We saw the release of fresh bindings for Gtk4.

- 🚀 [Writing beautiful GUI with Common Lisp and GTK4](https://github.com/bohonghuang/cl-gtk4)

We had bindings for Qt5… but they are still very rough, hard to install so far.

- [CommonQt 5](https://www.reddit.com/r/Common_Lisp/comments/s3xr8e/commonqt_5/) - Qt5 bindings.

Also:

- [I made a Wayland client from scratch for Common Lisp](https://www.reddit.com/r/Common_Lisp/comments/xz2ibr/i_made_a_wayland_client_from_scratch_for_common/)
- [vk](https://github.com/JolifantoBambla/vk) - CFFI bindings to Vulkan

History:

- [Izware Mirai is available on the Internet Archive](https://archive.org/details/izware_mirai_1.1sp2) (no source code)
  - [Hacking Lisp on Mirai](https://www.reddit.com/r/lisp/comments/wcyzis/hacking_lisp_on_mirai/) (screencast)

But an awesome novelty of 2022 is Kons-9.

### Kons-9, a new 3D graphics project

🚀 A new 3D graphics project: **Kons-9**.

- [reddit announce on August](https://www.reddit.com/r/lisp/comments/weocc5/new_open_source_common_lisp_3d_graphics_project/)
  - [HN comments (80)](https://news.ycombinator.com/item?id=32337538)

> The idea would be to develop a system along the lines of Blender/Maya/Houdini, but oriented towards the strengths of Common Lisp.

> I'm an old-time 3D developer who has worked in CL on and off for many years.

> I don't consider myself an expert […] A little about me: • wrote 3D animation software used in “Jurassic Park” • software R&D lead on “Final Fantasy: The Spirits Within” movie • senior software developer on “The Hobbit” films.

- [kons-9, a Common Lisp 3D Graphics Project ](https://github.com/kaveh808/kons-9)
- 🎥 [trailer](https://youtu.be/i0CwhEDAXB0)
- [author's blog posts](https://kaveh808.medium.com/once-more-into-the-breach-a-new-common-lisp-3d-graphics-project-d401bfa083d)
- and see also his screencasts below.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/i0CwhEDAXB0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Interfaces with other languages

- [py4cl2-cffi](https://github.com/digikar99/py4cl2-cffi): CFFI based alternative to py4cl2.
  - it does one big new thing: it supports passing CL arrays by
    reference. That means we actually have access to numpy, scipy, and
    friends.
  - "If py4cl2-cffi reaches stability, and I find that the performance
    of (i) cffi-numpy, (ii) magicl, as well as (iii) a few BLAS
    functions I have handcrafted for numericals turn out to be
    comparable, I might no longer have to reinvent numpy." @digikar
- [Small update to RDNZL (CL .NET bridge by Edi Weitz)](https://www.reddit.com/r/Common_Lisp/comments/yzoslr/small_update_to_rdnzl_cl_net_bridge_by_edi_weitz/)
  - forked project, added support for Int16, fixed Int64, re-building the supporting DLLs.
  - see also: [Bike](https://github.com/Lovesan/bike/)
- [jclass: Common Lisp library for Java class file manipulation](https://github.com/davidsun0/jclass)

For more, see awesome-cl.

## Numerical and scientific

- 🚀 [new Lisp Stats release](https://lisp-stat.dev/blog/releases/)
  - "emphasis on plotting and polishing of sharp edges. data-frames, array operations, documentation."
  - [HN comments (55)](https://news.ycombinator.com/item?id=32175820)
    - " I've been using lisp-stat in production as part of an algorithmic trading application I wrote. It's been very solid, and though the plotting is (perhaps was, in light of this new release) kinda unwieldy, I really enjoyed using it. Excited to check out the newest release."
    - "For example, within Lisp-Stat the statistics routines [1] were written by an econometrician working for the Austrian government (Julia folks might know him - Tamas Papp). It would not be exaggerating to say his job depending on it. These are state of the art, high performance algorithms, equal to anything available in R or Python. So, if you're doing econometrics, or something related, everything you need is already there in the tin."
    - "For machine learning, there's CLML, developed by NTT. This is the largest telco in Japan, equivalent to ATT in the USA. As well, there is MGL, used to win the Higgs Boson challenge a few years back. Both actively maintained."
    - "For linear algebra, MagicCL was mention elsewhere in the thread. My favourite is MGL-MAT, also by the author of MGL. This supports both BLAS and CUBLAS (CUDA for GPUs) for solutions."
    - "Finally, there's the XLISP-STAT archive. Prior to Luke Tierney, the author of XLISP-Stat joining the core R team, XLISP-STAT was the dominate statistical computing platform. There's heaps of stuff in the archive, most at least as good as what's in base R, that could be ported to Lisp-Stat."
    - "Common Lisp is a viable platform for statistics and machine learning. It isn't (yet) quite as well organised as R or Python, but it's all there."
- [numericals - Performance of NumPy with the goodness of Common Lisp](https://www.reddit.com/r/lisp/comments/wei81o/numericals_performance_of_numpy_with_the_goodness/)
  - `numericals` is "a high performance basic math library with
    vectorized elementary and transcendental functions. It also provides
    support for a numpy-like array object through `dense-arrays` and
    static-dispatch through the CLTL2 API."
  - the post is about numericals, dense-arrays, magicl, numcl, py4cl/2…
  - the author's [comparison and wish-list of features for a Common Lispy approach to a (better) Numpy](https://gist.github.com/digikar99/ba2f0bb34021bfdc086b9c1c712ca228)
- [MGL-MAT](https://github.com/melisgl/mgl-mat/) - a library for working with multi-dimensional arrays which supports efficient interfacing to foreign and CUDA code. BLAS and CUBLAS bindings are available.
- [hbook](https://github.com/eigenhombre/hbook) - Text-based histograms in Common Lisp inspired by the venerable HBOOK histogramming library from CERN.

New releases:

- [Maxima 5.46 was released](https://sourceforge.net/p/maxima/code/ci/master/tree/changelogs/ChangeLog-5.46.md).
  - "Maxima is a Computer Algebra System comparable to commercial systems like Mathematica and Maple. It emphasizes symbolic mathematical computation: algebra, trigonometry, calculus, and much more."
  - see its frontends, for example [WxMaxima](https://wxmaxima-developers.github.io/wxmaxima/).

Call to action:

- 📢 [Uncle Stats Wants You](https://www.reddit.com/r/Common_Lisp/comments/w6noju/uncle_stats_wants_you/)

## Web

[Screenshotbot](https://screenshotbot.io/) ([Github](https://github.com/screenshotbot/screenshotbot-oss)) was released. It is "a screenshot testing service to tie with your existing Android, iOS and Web screenshot tests".

It is straightforward to install with a Docker command. They offer more features and support with their paid service.

<img src="https://camo.githubusercontent.com/22657cfd6c85d2ef97bd6612b051949f8a1a856b3fa88b48f540d974576a4f88/68747470733a2f2f73637265656e73686f74626f742e696f2f6173736574732f696d616765732f6c6f676f2d6461726b2e706e67" style="max-width: 400px"/>

[LicensePrompt](https://www.licenseprompt.com/) was released. It is "a single place to track all recurring software and IT expenses and send relevant reminders to all interested people". It's built in CL, interface with HTMX.

- [Lisp Interpreter in a browser using **WASM**](https://vishpat.github.io/lisp-rs-wasm/)
- [	Show HN: Common Lisp running natively over WebAssembly for the first time](https://soi-disant.srht.site/entries/back-to-space.html)
  - [HN comments (64)](https://news.ycombinator.com/item?id=31590819)

Libraries:

- [jingle](https://www.reddit.com/r/lisp/comments/zgz8x8/jingle_common_lisp_web_framework_with_bells_and/): Common Lisp web framework with bells and whistles (based on ningle)
  - [jingle demo](https://www.reddit.com/r/lisp/comments/zk4jpl/jingle_demo_openapi_3x_spec_swagger_ui_docker_and/): OpenAPI 3.x spec, Swagger UI, Docker and command-line interface app with jingle.
- [ciao](https://github.com/kjinho/ciao): Ciao is an easy-to-use Common Lisp OAuth 2.0 client library. It is a port of the Racket OAuth 2.0 Client to Common Lisp.
- [stepster](https://github.com/walpurgisnatch/stepster/): a web scraping library, on top of Plump and Clss (new in QL)
- [openrpc](https://github.com/40ants/openrpc): Automatic OpenRPC spec generation, automatic JSON-RPC client building
- [HTTP/2 implementation in Common Lisp](https://github.com/zellerin/http2/)

Skeletons:

- [cl-cookieweb](https://github.com/vindarel/cl-cookieweb): my project skeleton to start web projects. Demo [in video](https://www.youtube.com/watch?v=XFc513MJjos). I am cheating, the bulk of it was done in 2021.
  - "Provides a working toy web app with the Hunchentoot web server, easy-routes, Djula templates, styled with Bulma, based on SQLite, with migrations and an example table definition."
  - if you don't know where to start for web dev in CL, enjoy all the pointers of this starter kit and find your best setup.
  - see also this [web template](https://dnaeon.github.io/common-lisp-project-template/) by @dnaeon, and check out all his other Lisp libraries.


Bindings:

- 👍 [lisp-pay](https://github.com/K1D77A/lisp-pay): Wrappers around various Payment Processors (Paypal, Stripe, Coinpayment)
- [lunamech-matrix-api](https://github.com/K1D77A/lunamech-matrix-api): Implementation of the Matrix API, LunaMech a Matrix bot

Apps:

<!-- - 📚 [OpenBookStore](https://github.com/OpenBookStore/openbookstore) -->
<!--   - a side project of mine, starts being usable. -->
- [Ackfock](https://www.ackfock.com/) - a platform of mini agreements and mini memos of understanding (built with CLOG, closed source).
- [todolist-cl](https://github.com/KikyTokamuro/todolist-cl): a nice looking todolist with a web UI, written in Common Lisp (and by a newcomer to CL, to add credit)

I don't have lots of open-source apps to show. Mines are running in production and all is going well. I share everything on my blog posts. I also have an open-source one in development, but that's for the 2023 showcase :D


### CLOG

🚀 The awesome novelty of 2022 I spoke of in the introduction is [CLOG](https://github.com/rabbibotton/clog/), the Common Lisp Omnificent GUI:

- [Native Desktop Executables for CLOG](https://github.com/rabbibotton/clog/tree/main/clogframe)
- [CLOG and CLOG Builder Release 1.8 - IDE for Common Lisp and CLOG](https://www.reddit.com/r/lisp/comments/xegotj/clog_and_clog_builder_release_18_ide_for_common/)
- [CLOG on Android, APK download](https://www.reddit.com/r/lisp/comments/tl46of/would_it_be_cool_to_run_a_clog_app_on_mobile_you/)

![The CLOG system browser](https://i.redd.it/5021ipbttce91.png)

I know of one open-source consequent CLOG app: [mold-desktop](https://codeberg.org/mmontone/mold-desktop/), in development.

> I'm developing a programmable desktop and a bookmarks manager application with CLOG. I think I know one of the things that make CLOG user interfaces so easy to develop. It is that they are effortlessly composable. That's it for now :)

@mmontone

<img src="https://preview.redd.it/18lns1ijizc91.png?width=1919&format=png&auto=webp&s=c103d33c22978ce8d2351de4cbf71638fe6286cf" style="max-width: 600px"/>


<!-- ## Other projects -->

<!-- - [Lisp Star: a star-shaped pendant that you can program in Lisp to make its six coloured LEDs twinkle in different patterns](http://www.technoblogy.com/show?2AMW) -->

## New releases

There are lots of awesome projects in music composition, including OpusModus and OpenMusic which saw new releases. I also like to cite [ScoreCloud](https://scorecloud.com/), a mobile app built with LispWorks, where you whistle, sing or play your instrument, and the app writes the music score O_o

- 🎵 [Opusmodus 3.0, Music Composition System, macOS Intel & Apple Silicon native, based on LispWorks, just released](https://opusmodus.com/)
- 🎵 [OpenMusic 7.0, now also native for M1 Macs, visual programming language designed for music composition](https://github.com/openmusic-project/openmusic/releases/tag/v7.0)
- [Consfigurator, a Common Lisp based declarative configuration management system, reaches v1.0](https://spwhitton.name/blog/entry/consfigurator_1.0.0/)
- [April 1.0 released](https://github.com/phantomics/april/releases/tag/v1.0) - APL in Common Lisp.
- [cl-cmark approaching stable release](https://hiphish.github.io/blog/2022/10/16/cl-cmark-approaching-stable/)
- [cl-git: a Common Lisp CFFI interface to the libgit2 library](https://github.com/russell/cl-git)
- [tinmop 0.9.9.1, a terminal client for gopher, gemini, kami and pleroma.](https://www.autistici.org/interzona/tinmop.html)
  - look here for how to build full-screen terminal applications in Common Lisp.
- [clingon - new release, new features (command-line options parser)](https://www.reddit.com/r/lisp/comments/tt1r9l/clingon_new_release_new_features_commandline/)
  - a full-featured options parser. Supports sub-commands, bash and zsh completions, many arguments types…

See awesome-cl and Cliki for more.

## (re) discoveries

- The math pastebin [Mathb.in is written in Common Lisp](https://mathb.in/3)
- [TIL that PTC's Creo Elements/Direct 3D CAD modeling software has a free version for Windows. "7+ million lines of Common Lisp code", used by Eterna for their watches.](https://www.reddit.com/r/Common_Lisp/comments/zg4qfy/til_that_ptcs_creo_elementsdirect_3d_cad_modeling/)
  - that's a huge software. 7 million lines. There's a free version to try out!

# Articles

## Graphics

- [Playing with raycasting](https://funcall.blogspot.com/2022/09/playing-with-raycasting.html)
- [Playing with graphics - a simple game with SDL2](https://funcall.blogspot.com/2022/08/playing-with-graphics.html)
- [Multipass Translator for CLIM ](https://turtleware.eu/posts/Multipass-Translator-for-CLIM.html)
- [McCLIM: Implementing a simpleminded REPL from scratch](http://turtleware.eu/posts/Implementing-a-simpleminded-REPL-from-scratch.html)

## Tooling

- [Debugging Lisp: trace options, break on conditions](https://www.reddit.com/r/Common_Lisp/comments/zakbpg/debugging_lisp_trace_options_break_on_conditions/)
  - [HN comments (28)](https://news.ycombinator.com/item?id=34252796#34279723), I learned, again, new tips.
- [Developing Common Lisp using GNU Screen, Rlwrap, and Vim](https://blog.djha.skin/p/developing-common-lisp-using-gnu-screen-rlwrap-and-vim/)
  - again, learned new tips. The `--remember` flag of `rlwrap` allows to TAB-complete whatever was previously typed at the prompt. That's dumb autocompletion, but autocompletion nonetheless. My [summary](https://github.com/vindarel/common-lisp-course-in-videos/blob/master/exercises/chapter%201%20-%20getting%20started/rlwrap-enhanced.md).
- [Configuring Slime cross-referencing](https://www.n16f.net/blog/configuring-slime-cross-referencing/)
- [SLIME Compilation Tips](https://www.n16f.net/blog/slime-compilation-tips/)
- [How to save lisp and die from Slime or Sly. trivial-dump-core](https://github.com/lisp-tips/lisp-tips/issues/38)
  - that may be super useful, at least if answers a question everybody has one time or another. I should try it more.
- [How to approach a Lisp sandbox environment](https://www.reddit.com/r/lisp/comments/uyubo1/how_to_approach_a_lisp_sandbox_environment/)
- [log4cl questions](https://www.reddit.com/r/Common_Lisp/comments/zqsv3w/log4cl_questions/)
- [A StumpWM debugger](https://www.reddit.com/r/stumpwm/comments/zgigdt/a_stumpwm_debugger/)
- [Windows environment for SBCL](https://www.reddit.com/r/lisp/comments/zfkiem/windows_environment_for_sbcl/)
- [Securing Quicklisp through mitmproxy](https://www.reddit.com/r/Common_Lisp/comments/thspgt/securing_quicklisp_through_mitmproxy/)
  - because Quicklisp doesn't use HTTPS. Here's how to add security. The new CLPM uses HTTPS.
- [Lisp in Vim](https://susam.net/blog/lisp-in-vim.html)

## Scripting

- [Day 3: Roswell: Common Lisp scripting](https://fukamachi.hashnode.dev/day-3-roswell-common-lisp-scripting), by E. Fukamachi.
- [Qlot tutorial with Docker](https://fukamachi.hashnode.dev/qlot-tutorial-with-docker)
  - [Qlot v1.0.0](https://github.com/fukamachi/qlot) was officially released.
- [Day 4: Roswell: How to make Roswell scripts faster](https://fukamachi.hashnode.dev/day-4-roswell-how-to-make-roswell-scripts-faster)
- [Day 5: Roswell: Hidden feature of "-s" option](https://fukamachi.hashnode.dev/day-5-roswell-hidden-feature-of-s-option)
- [Writing scripts in lisp?](https://www.reddit.com/r/lisp/comments/zwtfak/writing_scripts_in_lisp/)
  - a legitimate question. Many tips.
  - where I show my very new and un-released [CIEL scripting](https://ciel-lang.github.io/CIEL/#/scripting) facility. Batteries included for Common Lisp. That's for 2023 but you can be an early adopter :p
- [Lisp for Unix-like systems](https://www.reddit.com/r/lisp/comments/zosu50/lisp_for_unixlike_systems/)
  - in particular, read [this answer by /u/lispm](https://www.reddit.com/r/lisp/comments/zosu50/lisp_for_unixlike_systems/j0owuqn/) to learn about past and present attempts and solutions.
  - will the OP manage to make WCL work? "WCL: Delivering efficient Common Lisp applications under Unix", an implementation "tailored to zillions of small Lisp programs. Make sure that much of Lisp is shared memory. Be able to create shared memory libraries/applications".
- [(SBCL) Is there a way to detect if lisp code was run by --script vs interactively?](https://www.reddit.com/r/lisp/comments/zopsxq/sbcl_is_there_a_way_to_detect_if_lisp_code_was/)


## Around the language

- 🚀 [Using Coalton to Implement a Quantum Compiler](https://coalton-lang.github.io/20220906-quantum-compiler/)
  - Coalton brings Haskell-like type checking on top of Common Lisp. Groundbreaking. They use it for their quantum compiler.
  - it is still under development and there is [their Discord](https://discord.com/invite/cPb6Bc4xAH) to talk about it.
- [Eliminating CLOS for a 4.5x speedup](https://www.reddit.com/r/Common_Lisp/comments/ztzekh/eliminating_clos_for_a_45x_speedup/)
  - sometimes it's easy, use a struct.
- [The empty list.](https://www.tfeb.org/fragments/2022/12/16/the-empty-list/) "Someone has been getting really impressively confused and cross on reddit about empty lists, booleans and so on in Common Lisp, which led us to a discussion about what the differences really are. Here’s a summary which we think is correct."
- [Are there any tutorials or examples people would recommend to get started with unit testing common lisp?](https://www.reddit.com/r/Common_Lisp/comments/zfna4y/are_there_any_tutorials_or_examples_people_would/)
- [Fun with Macros: Do-File](https://www.reddit.com/r/Common_Lisp/comments/wylmfx/fun_with_macros_dofile/)
- [Series tips and tricks](http://funcall.blogspot.com/2022/07/series-tips-and-tricks.html)
- [STATIC-LET, Or How I Learned To Stop Worrying And Love LOAD-TIME-VALUE](https://github.com/phoe/articles/blob/main/2022-01-29-static-let/static-let.md)

History:

- [Old LISP programs still run in Common Lisp](http://informatimago.free.fr/i/develop/lisp/com/informatimago/small-cl-pgms/wang.html)

## Web related

- [Common Lisp web development and the road to a middleware](http://dnaeon.github.io/common-lisp-web-dev-ningle-middleware/)
- 🚀 [Lisp for the web: building one standalone binary with foreign libraries, templates and static assets](https://www.reddit.com/r/lisp/comments/z8s9o8/lisp_for_the_web_building_one_standalone_binary/)
  - what I managed to do here, with the help of the community, represents a great step forward for my Lisp web stack.
  - I can build a standalone binary for my web app, containing all static assets (a patch was required for the Djula HTML templating engine), so I can just `rsync` it to my server and it works.
  - a standalone binary is easy to integrate into an Electron window. Stay tuned.
- [Lisp for the web: deploying with Systemd, gotchas and solutions](https://www.reddit.com/r/lisp/comments/xetci2/lisp_for_the_web_deploying_with_systemd_gotchas/)
  - all the little gotchas I faced are now google-able.
- [Woo: a high-performance Common Lisp web server](https://fukamachi.hashnode.dev/woo-a-high-performance-common-lisp-web-server), by E. Fukamachi.
- [HTTP over unix sockets in Common Lisp](https://dev.to/rajasegar/http-over-unix-sockets-in-common-lisp-4l72). By the same author:
  - [Using TailwindCSS in Common Lisp web apps without Node.js tooling](https://dev.to/rajasegar/using-tailwindcss-in-common-lisp-web-apps-without-nodejs-tooling-3a8m)
  - [Using SVGs in Common Lisp web apps with Djula](https://dev.to/rajasegar/using-svgs-in-common-lisp-web-apps-with-djula-5f00)
  - [Create a Common Lisp Web app using ningle](https://dev.to/rajasegar/create-a-common-lisp-web-app-using-ningle-1oj7)
  - [Using environment variables with cl-dotenv in Common Lisp](https://dev.to/rajasegar/how-to-use-environment-variables-with-cl-dotenv-in-a-common-lisp-web-app-5eb5)
  - [Mito: An ORM for Common Lisp](https://dev.to/rajasegar/mito-an-orm-for-common-lisp-1n8n)
  - [Running docker commands from Common Lisp REPLs](https://dev.to/rajasegar/running-docker-commands-from-common-lisp-repls-350p)
  - he also put up several little examples of a Common Lisp web app with HTMX, such as [cl-beer](https://github.com/rajasegar/cl-beers). His stack: Caveman, Djula templates, HTMX for the interactivity. Thanks again, they are super useful.
- [A new static site generator](https://hiphish.github.io/blog/2022/10/02/a-new-static-site-generator/)
- [Web Development with CL Backend and ClojureScript Frontend](https://www.reddit.com/r/lisp/comments/ze2n7u/web_development_with_cl_backend_and_clojurescript/)
- [Update on gRPC Server](https://www.reddit.com/r/lisp/comments/wr0b6c/update_on_grpc_server/) - gRPC now has the ability to create servers based on protocol buffer service descriptors.
- 🚀 [Almost 600,000 entries per second from Lisp to Apache Accumulo over Apache Thrift](https://observablehq.com/@m-g-r/almost-600000-entries-per-second-from-lisp-to-accumulo)
- [Writing an interactive web app in Common Lisp: Hunchentoot then CLOG](https://lisp-journey.gitlab.io/blog/clog-contest/)
- [Review of CL Json Libraries, updated 15 Jan 2022](https://sabracrolleton.github.io/json-review.html)

Call for action:

- 📢 [JACL call for collaboration](https://www.reddit.com/r/Common_Lisp/comments/ykbi3x/jacl_call_for_collaboration/)

## Other articles

- [Astronomical Calculations for Hard Science Fiction in Common Lisp, by Fernando Borretti](https://borretti.me/article/astronomical-calculations-for-hard-sf-common-lisp)
  - [HN comments (42)](https://news.ycombinator.com/item?id=34058658) (yes, 42)
  - we miss you Fernando. Oh, he just released a new library: [lcm](https://github.com/eudoxia0/lcm): "Manage your system configuration in Common Lisp. Think of it as Ansible, for your localhost"
- 👀 [A LISP REPL Inside ChatGPT](https://maxtaylor.dev/posts/2022/12/lisp-repl)
- [FreeBSD Jail Quick Setup with Networking for a Common Lisp environment](https://www.shaka.today/freebsd-jail-quick-setup-with-networking-2022/)
- [Proceedings](https://arxiv.org/html/2205.11103), Seventeenth International Workshop on the **ACL2 Theorem Prover** and its Applications, 2022.
  - [ACL2 v8.5](https://www.cs.utexas.edu/users/moore/acl2/v8-5/combined-manual/index.html?topic=ACL2____NOTE-8-5) was released this year too.
- [Case study - house automation tool - part 2 - getting serial](http://retro-style.software-by-mabe.com/blog/House+automation+tooling+-+Part+2+-+Getting+Serial)

# Screencasts and podcasts

New videos by me:

- 🚀 [Debugging Lisp: fix and resume a program from any point in stack 🎥](https://lisp-journey.gitlab.io/blog/debugging-lisp-fix-and-resume-a-program-from-any-point-in-stack/)
- [How to request the GitHub API: demo of Dexador, Jonathan, Shasht (and Serapeum), livecoding in Emacs and SLIME.](https://www.youtube.com/watch?v=TAtwcBh1QLg)
- [How to create, run, build and load a new Lisp project](https://youtu.be/XFc513MJjos), with my fully-featured project skeleton.
- [Interactively Fixing Failing Tests in Common Lisp](https://www.youtube.com/watch?v=KsHxgP3SRTs)


by Gavin Freeborn:

- [Why Learn Lisp In 2022?](https://youtu.be/GWdf1flcLoM)
- [Why Are Lisp Macros So Great!?](https://youtube.com/watch?v=M4qj2ictRpg&feature=share)
- [Lisp Is More Than Just A Language It's An Environment](https://www.youtube.com/watch?v=AfY_zGR_QBI)
- [Rewrite Your Scripts In LISP - with Roswell](https://youtube.com/watch?v=QHwghMNKVtg&feature=share)
- [Creating Your First Lisp Project - Quicklisp, asdf, and Packages](https://youtu.be/LqBbGFMPcDI)
- [Unleash the REPL with SLY](https://youtube.com/watch?v=0DLdQ6yb7h8)
- [Lem: what if Emacs was multithreaded](https://youtu.be/Ph8M8ThBgPc)

KONS-9 series:

- [Kaveh's Common Lisp Lesson 01: points and shapes](https://www.youtube.com/watch?v=nSJcuOLmkl8)
- [Common Lisp OpenGL programming tutorial #12 - Animation & Hierarchies](https://youtu.be/WFa4Xqcisrs)

CLOG series:

  - [CLOG Extra 3 - The CLOG Project System for CLOG and NON-CLOG projects](https://youtube.com/watch?v=jEBDwjMnFXE)
  - [CLOG Extra 4 - All about panels](https://youtube.com/watch?v=CgTJMxsz3EY)
  - [Repl Style. Dev _visually_ with CLOG Builder](https://www.reddit.com/r/Common_Lisp/comments/w4zwm3/common_lisp_repl_style_dev_visually_with_clog/)
    - "This is amazing work! Having Pharo/Smalltalk capabilities with Common Lisp is fascinating."

CL study group:

- [Roswell (part II)](https://www.youtube.com/watch?v=oEjdONXZP1o)

<!-- by Josh Betts: -->

<!-- - [A CL website from scratch](https://youtu.be/f71d5Og0jIo)  No alt-right frog mascot, thanks. -->

Others:

- [Common Lisp Game of Life visualisation with LispWorks and CAPI](https://www.youtube.com/watch?v=RCWiqrdGcOs)
- [Nyxt on GambiConf (The hacker's power-browser, written in Common Lisp)](https://youtu.be/wCHnb8pvneE)
- [Far and Away the Simplest Tutorial on Macros](https://www.youtube.com/watch?v=UQZusJynjiM)

and of course, find 3h48+ of condensed Lisp content [on my Udemy video course](https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358)! (I'm still working on new content, as a student you get updates).

Aside screncasts, some **podcasts**:

- [CORECURSIVE #076: LISP in Space, With Ron Garret ← a podcast, trascribed with pictures](https://corecursive.com/lisp-in-space-with-ron-garret/)
  - [HN comments (98)](https://news.ycombinator.com/item?id=31234338)
  - same topic: [NASA Programmer Remembers Debugging Lisp in Deep Space](https://thenewstack.io/nasa-programmer-remembers-debugging-lisp-in-deep-space/)
- [The Array Cast - Andrew Sengul, creator of the April language tells us about the advantages of combining Lisp and APL](https://www.arraycast.com/episodes/episode23-andrew-sengul)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jBBS4FeY7XM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


# Other discussions

## Community

- 💌 [Impressions of Common Lisp and the Lisp Community](https://www.reddit.com/r/Common_Lisp/comments/y6xpxr/impressions_of_common_lisp_and_the_lisp_community/)
  - "So, thank you. I’m glad I found this group (and some other Lisp groups) on Reddit. You guys are a real blessing."
- [CL Community(?) Norms](https://www.timmons.dev/posts/cl-community-norms.html)
- [In what domains is common lisp used in 2022?](https://www.reddit.com/r/lisp/comments/zanm37/in_what_domains_is_common_lisp_used_2022/)
- [What are you working on?](https://www.reddit.com/r/Common_Lisp/comments/zx9qnq/what_are_you_working_on/)
- [A brief interview with Common Lisp creator Dr. Scott Fahlman](https://www.reddit.com/r/lisp/comments/ystf9s/a_brief_interview_with_common_lisp_creator_dr/)

## Learning Lisp

- [Advice on professional development in Common Lisp?](https://www.reddit.com/r/Common_Lisp/comments/zagmm3/advice_on_professional_development_in_common_lisp/)
- [Trying to get into Lisp, Feeling overwhelmed](https://www.reddit.com/r/lisp/comments/z4rev7/trying_to_get_into_lisp_feeling_overwhelmed/)
- [Looking for good common lisp projects on github to read?](https://www.reddit.com/r/lisp/comments/yydgrf/looking_for_good_common_lisp_projects_on_github/)
  - this [GitHub advanced search](https://github.com/search?p=2&q=defclass+user%3A40ants+user%3Aruricolist+user%3Ashinmera+user%3Ashirakumo+user%3Aedicl+language%3A%22Common+Lisp%22&type=Code) searches for `defclass` in 4 users projects: Shinmera, 40ants, Ruricolist, edicl (maintained Edi Weitz libraries):
- [Teach lisp to high schoolers?](https://www.reddit.com/r/lisp/comments/yw8jgl/teach_lisp_to_high_schoolers/)
- [Why buy LispWorks?](https://www.reddit.com/r/Common_Lisp/comments/yq2t4r/why_buy_lispworks/)
  - it's expensive (there's a free limited edition), but all this feedback is much interesting.
- [Writing robust software in CL](https://www.reddit.com/r/Common_Lisp/comments/xqibzx/writing_robust_software_in_cl/)
  - talking Erlang, Actors libraries.
  - [Moira](https://github.com/ruricolist/moira) -  Monitor and restart background threads.
- [What should a new programmer writing in Common Lisp know about security?](https://www.reddit.com/r/lisp/comments/vz9l55/what_should_a_new_programmer_writing_in_common/)

## Common Lisp VS …

- [what were the lessons you learned from programming in Lisp that carried over to other languages (more on the imperative side)?](https://www.reddit.com/r/lisp/comments/z69ko5/what_were_the_lessons_you_learned_from/)
- [For serious/industrial/business use, in what ways do you think Common Lisp beat Clojure, and vice versa?](https://www.reddit.com/r/lisp/comments/xo2ue7/for_seriousindustrialbusiness_use_in_what_ways_do/)
  - and: [When should I choose Common Lisp over Clojure (for business), and vice versa?](https://www.reddit.com/r/lisp/comments/wn7qhr/when_should_i_choose_common_lisp_over_clojure_for/)
- [Common Lisp VS Racket](https://gist.github.com/vindarel/c1ef5e043773921e3b11d8f4fe1ca7ac) (openly biased over CL, with testimonies of lispers knowing both)
  - [HN comments (143)](https://news.ycombinator.com/item?id=32723784):  "I'm a heavy user of Common Lisp, and I only dabble in Racket from time to time. While Common Lisp is my tool of choice for a lot of reasons stated in the post, since the post largely skews favorably toward Common Lisp, I'll offer two things that Racket shines at."
- [Why not: from Common Lisp to Julia](https://gist.github.com/digikar99/24decb414ddfa15a220b27f6748165d7)
  - [HackerNews comments (200)](https://news.ycombinator.com/item?id=32745318) (for this post and the post it responds to)
  - The author of the first post said he was "migrating from Common Lisp to Julia as [his] primary programming language". He was starting "to grow increasingly frustrated with various aspects of the language" and the CL ecosystem. Many of his arguments were harsh towards CL and didn't mention existing better alternatives.
  - Two months later, in [another blog
    post](https://mfiano.net/posts/2022-11-05-programming-languages-i-use/),
    he admits the previous article "was unfair to both languages", and
    while "[he] is still mostly using Julia", "Common Lisp is still on the table for [him]".
  - On
    [LiberaChat](https://irclog.tymoon.eu/libera/%23commonlisp?around=1669837028#1669837028),
    he says he's back from Julia to Common Lisp: "As of yesterday, I'm
    back to making libraries in CL. I cannot consider Julia as a
    serious language anymore, for it has deceived me after 7 years of
    research."
  <!-- - In 2017, he tried migrating to Racket, but also came back to CL. -->
  - Welcome back and thanks to him for the past and future CL libraries.
- [Is Lisp too malleable to use for serious development?](https://www.reddit.com/r/lisp/comments/wgdy51/is_lisp_too_malleable_to_use_for_serious/)
- [Why Lisp?](https://github.com/naver/lispe/wiki/6.16-Why-Lisp) ([version française](https://github.com/naver/lispe/wiki/6.16-Pourquoi-Lisp)). "We will show how our own LispE interpreter was implemented."
  - [HN comments (200)](https://news.ycombinator.com/item?id=33462454)

---

Thanks everyone, happy lisping and see you around!
