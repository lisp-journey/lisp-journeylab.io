Documenting my journey. Many findings go on the [CL Cookbook](https://lispcookbook.github.io/cl-cookbook/) and in the [Awesome-cl](https://github.com/CodyReichert/awesome-cl) list too.

[https://lisp-journey.gitlab.io/](https://lisp-journey.gitlab.io/)

[https://gitlab.com/lisp-journey/lisp-journey.gitlab.io/](https://gitlab.com/lisp-journey/lisp-journey.gitlab.io/)

# Contribute

This is a [Hugo](https://gohugo.io/) static website.

Run it locally:

    make run # hugo serve

## Info boxes

To include info boxes, we use raw HTML:

{{< rawhtml >}}
<div class="info" style="background-color: #e7f3fe; border-left: 6px solid #2196F3; padding: 17px;">
<strong>INFO:</strong>
dear readers, here's <a href="https://www.udemy.com/course/common-lisp-programming/?couponCode=LISP-JOURNEY-220915"> the best price I can set for my Udemy course "Learn Lisp effectively" </a>, it will be available for 5 days! Thanks all for your support (NB: I'm working on the chapter on condition handling and new chapters are available for existing students).
</div>
{{< /rawhtml >}}

{{< rawhtml >}}
<div class="info" style="background-color: #e7f3fe; border-left: 6px solid #2196F3; padding: 17px; margin-top: 1em;">
<strong>INFO:</strong>
foo <a href="link"> link </a>, bar.
</div>
{{< /rawhtml >}}